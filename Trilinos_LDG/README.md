\mainpage  

\author Michael Harmon

\section Overview

\subsection Introduction
This code is designed to numerically solve the Poisson equation using the
local discontinuous Galerkin (LDG) method (See LDG::LDGPoissonProblem).  It uses the
deal.ii finite element library as well as the Trilinos library for 
access to distributed linear algebra package.
Access to Trilinos allows for massively distributed computations using the Message
Passing Interface (MPI).  The domain, mesh and location of boundary conditions are
set in Grid


\subsection Requirements 
The requirements for this software is 
<a href="https://www.dealii.org">deal.ii library</a> version 8.3.0 or higher,
<a href="https://www.cmake.org">CMake</a> version 2.8 or higher,
MPI
Trillinos


\subsection Installation
First obtain and install a copy of the dealii
<a href="https://www.dealii.org">deal.ii library</a> version 8.3.0 or higher. 
See the dealii library for installation instructions.

\subsection Model
The Poisson equation we solve is,
\f[
\begin{align}
-\nabla^{2} u &= f(x) && \text{in} \; \Omega \\
\textbf{n} \cdot \nabla u &= g_{N}(x) && \text{on} \; \partial \Omega_{N} \\
u &= g_{D}(x) && \text{on} \; \partial \Omega_{D} 
\end{align}
\f]


\subsection	Compiling
To generate a makefile for this code using CMake type into the terminal:

<code> cmake . -DDEAL_II_DIR=/path_to_deal.ii <code>

You can compile the debug mode use:

<code> make <code>

You can compile the release mode use:

<code> make release <code>

Either of these commands will make the executable <code> main <code>.


\subsection	Using

To run the code on N processor use the command:

<code> mpirun -np N ./main <code>


\subsection Questions
Email: mdh266@gmail.com



