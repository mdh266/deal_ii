#ifndef _TEST_FUNCTIONS_H__
#define _TEST_FUNCTIONS_H__

#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/lac/vector.h>
#include <cmath>

/*---------------------------------------------------------------------*/
/*									POISSON TEST																			 */
/*---------------------------------------------------------------------*/

namespace test_Poisson
{
	using namespace dealii;

	template <int dim>
	class RightHandSide : public Function<dim>
	{
		public:
			RightHandSide() : Function<dim>(1)
			{}
			
			virtual double value(const Point<dim> &p, 
								 const unsigned int component = 0 ) const;
	};

	template <int dim>
	class DirichletBoundaryValues : public Function<dim>
	{
		public:
			DirichletBoundaryValues() : Function<dim>(1)
			{}
			
			virtual double value(const Point<dim> &p, 
								 const unsigned int component = 0 ) const;
	};

	template<int dim>
	class TrueSolution : public Function<dim>
	{
		public:
			TrueSolution() : Function<dim>(dim+1)
			{}

			virtual void vector_value(const Point<dim> & p,
									  Vector<double> &valuess) const;
	};

} // end test_Poisson namespace

#endif