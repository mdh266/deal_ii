///
// Poisson solver using Local Discontinuous Galerkin Method
//
//
#ifndef _POISSON_LDG_H__
#define _POISSON_LDG_H__

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/base/timer.h>
#include <deal.II/base/convergence_table.h>

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/compressed_sparsity_pattern.h>


#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_dgq.h> // Lagrange dg fe elements
#include <deal.II/fe/fe_dgp.h> // Legendre dg fe elements
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>


// parallel stuff
#include <deal.II/base/utilities.h>
#include <deal.II/base/index_set.h>
#include <deal.II/base/conditional_ostream.h>

#include <deal.II/lac/sparsity_tools.h>

#include <deal.II/distributed/tria.h>

#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/trilinos_vector.h>
#include <deal.II/lac/trilinos_solver.h>
#include <deal.II/lac/trilinos_precondition.h>

#include <fstream>
#include <iostream>

#include "../include/Grid.hpp"
#include "../include/test_functions.hpp"

namespace LDG
{
using namespace dealii;

///////////////////////////////////////////////////////////////////////////////
// LDG POISSON CLASS
///////////////////////////////////////////////////////////////////////////////


template <int dim>
class LDGPoissonProblem
{
public:
    LDGPoissonProblem(const unsigned int degree,
                      const unsigned int n_refine);

    ~LDGPoissonProblem();

    void run(ConvergenceTable & error_table);


private:
    void make_dofs_and_allocate_memory();
    void assemble_system();
    void assemble_cell_terms(const FEValues<dim> 	&cell_fe,
                             FullMatrix<double> 	&cell_matrix,
                             Vector<double>			&cell_vector);

    void assemble_Neumann_boundary_terms(const FEFaceValues<dim> 	&face_fe,
                                         FullMatrix<double> 		&local_matrix,
                                         Vector<double>				&local_vector);

    void assemble_Dirichlet_boundary_terms(const FEFaceValues<dim> 	&face_fe,
                                           FullMatrix<double> 		&local_matrix,
                                           Vector<double>			&local_vector);

    void assemble_flux_terms(const FEFaceValuesBase<dim> 	&fe_face_values,
                             const FEFaceValuesBase<dim>	&fe_neighbor_face_values,
                             FullMatrix<double> 			&vi_ui_matrix,
                             FullMatrix<double>				&vi_ue_matrix,
                             FullMatrix<double>				&ve_ui_matrix,
                             FullMatrix<double>				&ve_ue_matrix);

    void solve();
	
	void compute_errors(double & density_error,
						double & flux_error);
 
	void output_results() const;

    const unsigned int degree;
    const unsigned int n_refine;
    double penalty;
    double h_max;
    double h_min;

	enum
	{
		Dirichlet,
		Neumann
	};

    parallel::distributed::Triangulation<dim>		triangulation;
    FESystem<dim>									fe;
    DoFHandler<dim>									dof_handler;

    ConstraintMatrix								constraints;

    SparsityPattern									sparsity_pattern;

    TrilinosWrappers::SparseMatrix					system_matrix;
    TrilinosWrappers::MPI::Vector					locally_relevant_solution;
    TrilinosWrappers::MPI::Vector					system_rhs;

    ConditionalOStream								pcout;
    TimerOutput										computing_timer;

    SolverControl									solver_control;
    TrilinosWrappers::SolverDirect					solver;


    const test_Poisson::RightHandSide<dim>				rhs_function;
    const test_Poisson::DirichletBoundaryValues<dim>	Dirichlet_bc_function;
    const test_Poisson::TrueSolution<dim>				true_solution;
};

}
#endif
