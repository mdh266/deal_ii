#include "../include/PoissonLDG.hpp"
#include "PoissonLDG.cpp"

int main(int argc, char *argv[])
{
    try {
        using namespace dealii;
        using namespace LDG;

        deallog.depth_console(0);

        Utilities::MPI::MPI_InitFinalize mpi_initialization(argc, argv,
               										 numbers::invalid_unsigned_int);

        int degree = 1;
        unsigned int min_refine = 1;
        unsigned int max_refine = 7;

		ConvergenceTable 		Error_table;

		// loop over the number of refinements and collect L2 errors in
		// convergence table
		for(unsigned int n_refine=min_refine; 
			n_refine < max_refine;
			n_refine++)
		{
       	 	LDGPoissonProblem<2> 	Poisson(degree, n_refine);
       		Poisson.run(Error_table);
		}

		// get the convergence rates for carrier_1 and carrier_2
		Error_table.evaluate_convergence_rates("u", 
							ConvergenceTable::reduction_rate_log2);
	
		Error_table.evaluate_convergence_rates("q", 
							ConvergenceTable::reduction_rate_log2);
	
		// set to 3 significant digits as output
		Error_table.set_precision("u", 3);
		Error_table.set_precision("q", 3);

		// use scinetific notation for output
		Error_table.set_scientific("u", true);
		Error_table.set_scientific("q", true);
			
		// on processor 0 print out the convergence results
		if(Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)==0)
		{	
			std::cout << std::endl;
			std::cout << "LDG Errors" << std::endl;	
			Error_table.write_text(std::cout);
		}

    }
    catch (std::exception &exc)
    {
        std::cerr << std::endl << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Exception on processing: " << std::endl
                  << exc.what() << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;

        return 1;
    }
    catch (...)
    {
        std::cerr << std::endl << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Unknown exception!" << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    }
    return 0;
}
