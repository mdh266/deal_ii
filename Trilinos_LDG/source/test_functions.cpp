#include "../include/test_functions.hpp"

/*---------------------------------------------------------------------*/
/*									POISSON TEST																			 */
/*---------------------------------------------------------------------*/
namespace test_Poisson
{
	template <int dim>
	double 
	RightHandSide<dim>::
	value(const Point<dim> &p, 
		const unsigned int ) const
	{
		const double x = p[0];
		const double y = p[1];
		return 4*M_PI*M_PI*(cos(2*M_PI*y) - sin(2*M_PI*x));

	}

	template <int dim>
	double
	DirichletBoundaryValues<dim>::
	value(const Point<dim> &p, 
		const unsigned int ) const
	{
		const double x = p[0];
		const double y = p[1];
		return cos(2*M_PI*y) -sin(2*M_PI*x) - x;
	}

	
	template <int dim>
	void 
	TrueSolution<dim>::
	vector_value(const Point<dim> &p,
				 Vector<double> &values) const
	{
		Assert(values.size() == dim+1,	
					 ExcDimensionMismatch(values.size(), dim+1) );

		double x = p[0];
		double y = p[1];

		//gradient values
		values(0) = 1 + 2*M_PI*cos(2*M_PI*x);
		values(1) = 2*M_PI*sin(2*M_PI*y);

		// function values
		values(2) = cos(2*M_PI*y) - sin(2*M_PI*x) - x;
	}

} // end test_Poisson namespace

