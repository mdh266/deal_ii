#include "../include/PoissonLDG.hpp"

namespace LDG
{
using namespace dealii;

template <int dim>
LDGPoissonProblem<dim>::
LDGPoissonProblem(const unsigned int degree,
					        const unsigned int n_refine)
    :
    degree(degree),
    n_refine(n_refine),
    triangulation(MPI_COMM_WORLD,
                 typename Triangulation<dim>::MeshSmoothing
                 (Triangulation<dim>::smoothing_on_refinement |
                  Triangulation<dim>::smoothing_on_coarsening)),
    fe( FESystem<dim>(FE_DGQ<dim>(degree), dim),		1,
       FE_DGQ<dim>(degree), 							1),
    dof_handler(triangulation),
    pcout(std::cout,
         Utilities::MPI::this_mpi_process(MPI_COMM_WORLD) == 0),
    computing_timer(MPI_COMM_WORLD,
                   pcout,
                   TimerOutput::summary,
                   TimerOutput::wall_times),
    solver_control(1),
    solver(solver_control),
    rhs_function(),
    Dirichlet_bc_function()
{
}

template <int dim>
LDGPoissonProblem<dim>::
~LDGPoissonProblem()
{
    dof_handler.clear();
}

template <int dim>
void 
LDGPoissonProblem<dim>::
make_dofs_and_allocate_memory()
{
    TimerOutput::Scope t(computing_timer, "setup");

    dof_handler.distribute_dofs(fe);

    // Renumber dofs for [ VectorField, Potential ]^{T} set up
    DoFRenumbering::component_wise(dof_handler);

    // get locally owned dofs
    IndexSet locally_owned_dofs = dof_handler.locally_owned_dofs();
		
	IndexSet locally_relevant_dofs;
    // get locally relevant dofs-> those do comp on, but need for comp
    // on locally owned
    DoFTools::extract_locally_relevant_dofs(dof_handler,
                                            locally_relevant_dofs);

    std::vector<types::global_dof_index> dofs_per_component(dim+1);
    DoFTools::count_dofs_per_component(dof_handler, dofs_per_component);

	// no constrains
    constraints.clear();
    constraints.close();

    // BASE OF OFF STEP-40

    //create sparsity pattern
    DynamicSparsityPattern dsp(dof_handler.n_dofs());
    DoFTools::make_flux_sparsity_pattern(dof_handler,
                                         dsp);

    // distribute sparsity pattern to other processors
    SparsityTools::distribute_sparsity_pattern(dsp,
            dof_handler.n_locally_owned_dofs_per_processor(),
            MPI_COMM_WORLD,
            locally_relevant_dofs);

    // initialize this processors matrix
    system_matrix.reinit(locally_owned_dofs,
                         locally_owned_dofs,
                         dsp,
                         MPI_COMM_WORLD);

    // initialize this processors solution
    locally_relevant_solution.reinit(locally_relevant_dofs,
                                     MPI_COMM_WORLD);

    // initialize this processors rhs
    system_rhs.reinit(locally_owned_dofs,
                      locally_relevant_dofs,
                      MPI_COMM_WORLD,
                      true);

    // get number of dofs in vector field components and of potential
    // in each component/dimension of vector field has same number of dofs
    const unsigned int n_vector_field = dim * dofs_per_component[0];
//		std::cout << "n_vector_field = " << n_vector_field << std::endl;

    const unsigned int n_potential = dofs_per_component[1];
//		std::cout << "n_potential = " << n_potential << std::endl;

    pcout << "Number of active cells : "
          << triangulation.n_global_active_cells()
          << std::endl
          << "h_{max} : "
          << h_max
          << std::endl
          << "Number of degrees of freedom: "
          << dof_handler.n_dofs()
          << " (" << n_vector_field << " + " << n_potential << ")"
          << std::endl;
}

template <int dim>
void 
LDGPoissonProblem<dim>::
assemble_system()
{
    TimerOutput::Scope t(computing_timer, "assembly");

    QGauss<dim>			quadrature_formula(fe.degree+1);
    QGauss<dim-1>		face_quadrature_formula(fe.degree+1);

    const UpdateFlags update_flags 	= update_values
                                      | update_gradients
                                      | update_quadrature_points
                                      | update_JxW_values;

    const UpdateFlags face_update_flags	=	update_values
                                            | update_normal_vectors
                                            | update_quadrature_points
                                            | update_JxW_values;

    FEValues<dim>			fe_values(fe, quadrature_formula, update_flags);

    FEFaceValues<dim>		fe_face_values(fe,face_quadrature_formula, face_update_flags);

    FESubfaceValues<dim>	fe_subface_values(fe, face_quadrature_formula, 
											face_update_flags);

    FEFaceValues<dim>		fe_neighbor_face_values(fe, face_quadrature_formula,
													face_update_flags);

    const unsigned int dofs_per_cell = fe.dofs_per_cell;

	FullMatrix<double>		local_matrix(dofs_per_cell,dofs_per_cell);
	Vector<double>			local_vector(dofs_per_cell);		

    // Flux integrals
    FullMatrix<double>		vi_ui_matrix(dofs_per_cell, dofs_per_cell);
    FullMatrix<double>		vi_ue_matrix(dofs_per_cell, dofs_per_cell);
    FullMatrix<double>		ve_ui_matrix(dofs_per_cell, dofs_per_cell);
    FullMatrix<double>		ve_ue_matrix(dofs_per_cell, dofs_per_cell);

    std::vector<types::global_dof_index> local_dof_indices(dofs_per_cell);
    std::vector<types::global_dof_index> local_neighbor_dof_indices(dofs_per_cell);

	// loop over all the cells and assemble the matrix and rhs vector
	typename DoFHandler<dim>::active_cell_iterator
	cell = dof_handler.begin_active(),
	endc = dof_handler.end();

	for(; cell!=endc; cell++)
  	{
		// only do computations on cells which are locally owned
    	// NOTE: This will use info on cells that are locally relevant
    	if(cell->is_locally_owned())
    	{
    		local_matrix = 0;
			local_vector = 0;

			fe_values.reinit(cell);

    		assemble_cell_terms(fe_values,
        					local_matrix,
        					local_vector);

      		// need this before to construct global flux matrices
     		cell->get_dof_indices(local_dof_indices);

      		// loop over all the faces of this cel/
     		for(unsigned int face_no=0; 
				face_no< GeometryInfo<dim>::faces_per_cell; 
				face_no++)
			{
                typename DoFHandler<dim>::face_iterator 	face = cell->face(face_no);
				
				// construct matricx for the bounary value flux
                if(face->at_boundary() )
                {
					/// update the fe_face_values for this face
                    fe_face_values.reinit(cell, face_no);
		
					// assemble Dirichlet terms
                    if(face->boundary_id() == Dirichlet)
                    {
                        assemble_Dirichlet_boundary_terms(fe_face_values,
                                                          local_matrix,
                                                          local_vector);
                    }
                    else if(face->boundary_id() == Neumann)
                    {
                        assemble_Neumann_boundary_terms(fe_face_values,
                                                        local_matrix,
                                                        local_vector);
                    }
                    else
                        Assert(false, ExcNotImplemented() );
                }
                else
                {
                    // now we are on the interior face elements
                    // we want to make sure that neighbor to this state is a valid
                    // cell
                    Assert(cell->neighbor(face_no).state() == IteratorState::valid,
                           ExcInternalError());

                    // get the neighbor cell which shares this face
                    typename DoFHandler<dim>::cell_iterator neighbor =
                        cell->neighbor(face_no);

                    // if face has children
                    // then the neighbor to this cell's face is more refined
                    // then it is.
                    if(face->has_children())
                    {
                        // face such that neighbor(neigh_face_no) = cell(face_no)
                        const unsigned int neighbor_face_no =
                            cell->neighbor_of_neighbor(face_no);


                        // loop over all the subfaces of this face
                        for(unsigned int subface_no=0;
                                subface_no < face->number_of_children();
                                ++subface_no)
                        {

                            // get the refined neighbor cell that matches this face
                            // and subface number
                            typename DoFHandler<dim>::cell_iterator neighbor_child =
                                cell->neighbor_child_on_subface(face_no, subface_no);

                            // parent cant be more than one level above the child
                            Assert(!neighbor_child->has_children(), ExcInternalError());


                            vi_ui_matrix = 0;
                            vi_ue_matrix = 0;
                            ve_ui_matrix = 0;
                            ve_ue_matrix = 0;

                            // reinitialize this fe_values to this cell's subface and
                            // neighbor_childs fe values to the face
                            // NOTE: this uses FESubfaceValues!!!!
                            fe_subface_values.reinit(cell, face_no, subface_no);
                            fe_neighbor_face_values.reinit(neighbor_child, neighbor_face_no);


                            assemble_flux_terms(fe_subface_values,
                                                fe_neighbor_face_values,
                                                vi_ui_matrix,
                                                vi_ue_matrix,
                                                ve_ui_matrix,
                                                ve_ue_matrix);


                            neighbor_child->get_dof_indices(local_neighbor_dof_indices);


                            constraints.distribute_local_to_global(vi_ui_matrix,
                                                                   local_dof_indices,
                                                                   system_matrix);

                            constraints.distribute_local_to_global(vi_ue_matrix,
                                                                   local_dof_indices,
                                                                   local_neighbor_dof_indices,
                                                                   system_matrix);

                            constraints.distribute_local_to_global(ve_ui_matrix,
                                                                   local_neighbor_dof_indices,
                                                                   local_dof_indices,
                                                                   system_matrix);

                            constraints.distribute_local_to_global(ve_ue_matrix,
                                                                   local_neighbor_dof_indices,
                                                                   system_matrix);

                        } // for subface_no

                    } // if face has children
                    else
                    {
                        // we know that the neighbor of this cell face is on the same
                        // refinement level and therefore, the
                        // cell with the lower index does the work
                        if(neighbor->level() == cell->level() &&
                                neighbor->index() > cell->index() )
                        {

                            // face such that neighbor(neigh_face_no) = cell(face_no)
                            const unsigned int neighbor_face_no =
                                cell->neighbor_of_neighbor(face_no);

                            vi_ui_matrix = 0;
                            vi_ue_matrix = 0;
                            ve_ui_matrix = 0;
                            ve_ue_matrix = 0;

                            // reinitiatiz the fe face values to be for this cell's face
                            // and neighbors face
                            fe_face_values.reinit(cell, face_no);
                            fe_neighbor_face_values.reinit(neighbor, neighbor_face_no);


                            assemble_flux_terms(fe_face_values,
                                                fe_neighbor_face_values,
                                                vi_ui_matrix,
                                                vi_ue_matrix,
                                                ve_ui_matrix,
                                                ve_ue_matrix);


                            neighbor->get_dof_indices(local_neighbor_dof_indices);


                            constraints.distribute_local_to_global(vi_ui_matrix,
                                                                   local_dof_indices,
                                                                   system_matrix);

                            constraints.distribute_local_to_global(vi_ue_matrix,
                                                                   local_dof_indices,
                                                                   local_neighbor_dof_indices,
                                                                   system_matrix);

                            constraints.distribute_local_to_global(ve_ui_matrix,
                                                                   local_neighbor_dof_indices,
                                                                   local_dof_indices,
                                                                   system_matrix);

                            constraints.distribute_local_to_global(ve_ue_matrix,
                                                                   local_neighbor_dof_indices,
                                                                   system_matrix);

                        } // if
                    } // else face doesnt have children
                } // else if interior face
            } // end for face_no

            constraints.distribute_local_to_global(local_matrix,
                                                   local_dof_indices,
                                                   system_matrix);

            constraints.distribute_local_to_global(local_vector,
                                                   local_dof_indices,
                                                   system_rhs);

        } // if locally_owned_cell
    } // end for cell

    // synchronize assembly with other processors
    system_matrix.compress(VectorOperation::add);
    system_rhs.compress(VectorOperation::add);
} 


template<int dim>
void 
LDGPoissonProblem<dim>::assemble_cell_terms(
    const FEValues<dim> 	&cell_fe,
    FullMatrix<double> 		&cell_matrix,
    Vector<double>				&cell_vector)
{
    const unsigned int dofs_per_cell = cell_fe.dofs_per_cell;
    const unsigned int n_q_points		 = cell_fe.n_quadrature_points;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);

    std::vector<double>							rhs_values(n_q_points);

    rhs_function.value_list(cell_fe.get_quadrature_points(),
                            rhs_values);

    for(unsigned int q=0; q<n_q_points; q++)
    {
        for(unsigned int i=0; i<dofs_per_cell; i++)
        {
            const Tensor<1, dim>  psi_i_field			= cell_fe[VectorField].value(i,q);
            const double 		  div_psi_i_field	    = cell_fe[VectorField].divergence(i,q);
            const double	 	  psi_i_potential	    = cell_fe[Potential].value(i,q);
            const Tensor<1, dim>  grad_psi_i_potential  = cell_fe[Potential].gradient(i,q);

            for(unsigned int j=0; j<dofs_per_cell; j++)
            {
                const Tensor<1, dim> psi_j_field		= cell_fe[VectorField].value(j,q);
                const double 		 psi_j_potential	= cell_fe[Potential].value(j,q);

                cell_matrix(i,j)  += ( (psi_i_field * psi_j_field)
                                       -
                                       (div_psi_i_field * psi_j_potential)
                                       -
                                       (grad_psi_i_potential * psi_j_field)
                                     ) * cell_fe.JxW(q);
            } // for j

            // contribution from RHS
            cell_vector(i) += psi_i_potential *
                              rhs_values[q] *
                              cell_fe.JxW(q);
        } // for i
    }	// for q
} // assemble cell


template<int dim>
void 
LDGPoissonProblem<dim>::
assemble_Dirichlet_boundary_terms(
    const FEFaceValues<dim> 	&face_fe,
    FullMatrix<double>			&local_matrix,
    Vector<double>				&local_vector)
{
    const unsigned int dofs_per_cell 	 = face_fe.dofs_per_cell;
    const unsigned int n_q_points		 = face_fe.n_quadrature_points;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);

    std::vector<double>		Dirichlet_bc_values(n_q_points);

    Dirichlet_bc_function.value_list(face_fe.get_quadrature_points(),
                                     Dirichlet_bc_values);

    for(unsigned int q=0; q<n_q_points; q++)
    {
        for(unsigned int i=0; i<dofs_per_cell; i++)
        {
            const Tensor<1, dim>  psi_i_field			 = face_fe[VectorField].value(i,q);
            const double 		  psi_i_potential		 = face_fe[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_per_cell; j++)
            {
                const Tensor<1, dim>  psi_j_field	  = face_fe[VectorField].value(j,q);
            		const double 		  psi_j_potential		 = face_fe[Potential].value(j,q);
                // flux for \hat{q}
                // int_{\Gamma_{D}} (v^{-} ( n^{-} * q^{-} + penalty * u^{-})  ds
                local_matrix(i,j) += psi_i_potential * (
                                     face_fe.normal_vector(q) *
                                     psi_j_field
									 + 
									 penalty *
									 psi_j_potential) *
                                     face_fe.JxW(q);

            } // for j

            /// flux for \hat{u}
            // int_{\Gamma_{D}} (-p^{-} n^{-} + \penalty * v^{-}) u_{D} ds
            local_vector(i) += (-1.0 * psi_i_field *
                            	face_fe.normal_vector(q) 
								+ 
								penalty *
								psi_i_potential) *
                               	Dirichlet_bc_values[q] *
                               	face_fe.JxW(q);
        } // for i
    }	// for q
} // end assemble_Dirichlet_boundary_terms()

template<int dim>
void
LDGPoissonProblem<dim>::
assemble_Neumann_boundary_terms(
    const FEFaceValues<dim> 	&face_fe,
    FullMatrix<double>			&local_matrix,
    Vector<double>				&local_vector)
{
    const unsigned int dofs_per_cell = face_fe.dofs_per_cell;
    const unsigned int n_q_points		 = face_fe.n_quadrature_points;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);

    std::vector<double >	Neumann_bc_values(n_q_points);

    for(unsigned int q=0; q<n_q_points; q++)
    {
        for(unsigned int i=0; i<dofs_per_cell; i++)
        {
            const Tensor<1, dim>  psi_i_field		 = face_fe[VectorField].value(i,q);
	        const double 		  psi_i_potential	 = face_fe[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_per_cell; j++)
            {

                const double 	psi_j_potential		 = face_fe[Potential].value(j,q);

                // flux for \hat{q}
                // int_{\Gamma_{N}} p^{-} n^{-} u^{-} ds
                local_matrix(i,j) += psi_i_field *
                                     face_fe.normal_vector(q) *
                                     psi_j_potential *
                                     face_fe.JxW(q);

            } // for j

            // flux for \hat{u}
            // int_{\Gamma_{N}} -v^{-} g_{N} ds
            local_vector(i) +=  -psi_i_potential *
								Neumann_bc_values[q] * 
								face_fe.JxW(q);	        
        } // for i
    }	// for q
}

template<int dim>
void LDGPoissonProblem<dim>::assemble_flux_terms(
    const FEFaceValuesBase<dim> 	&fe_face_values,
    const FEFaceValuesBase<dim>		&fe_neighbor_face_values,
    FullMatrix<double> 				&vi_ui_matrix,
    FullMatrix<double>				&vi_ue_matrix,
    FullMatrix<double>				&ve_ui_matrix,
    FullMatrix<double>				&ve_ue_matrix)
{
    const unsigned int n_face_points 	  =	fe_face_values.n_quadrature_points;
    const unsigned int dofs_this_cell 	  = fe_face_values.dofs_per_cell;
    const unsigned int dofs_neighbor_cell =	fe_neighbor_face_values.dofs_per_cell;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);

    // beta would be for giving the LDG/ALternating fluxes
    Point<dim> beta;
    for(int i=0; i<dim; i++)
        beta(i) = 1.0;
    beta /= sqrt(beta.square() );

    for(unsigned int q=0; q<n_face_points; q++)
    {
        for(unsigned int i=0; i<dofs_this_cell; i++)
        {
            const Tensor<1,dim>  psi_i_field_minus	=
                						fe_face_values[VectorField].value(i,q);
            const double psi_i_potential_minus	=
              				  			fe_face_values[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_this_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_minus	=
                    					fe_face_values[VectorField].value(j,q);
                const double psi_j_potential_minus	=
                			   			 fe_face_values[Potential].value(j,q);

                // int_{face} n^{-} * ( p_{i}^{-} u_{j}^{-} + v^{-} q^{-} ) dx
                // 					  + penalty v^{-}u^{-} dx
                vi_ui_matrix(i,j)	+= (0.5 * (
                                    	psi_i_field_minus *
                                    	fe_face_values.normal_vector(q) *
                                    	psi_j_potential_minus
                                    	+
                                    	psi_i_potential_minus *
                                    	fe_face_values.normal_vector(q) *
                                      	psi_j_field_minus )
										+
										beta *
										psi_i_field_minus *
										psi_j_potential_minus
										-
										beta *
										psi_i_potential_minus *
										psi_j_field_minus
										+	
										penalty *
										psi_i_potential_minus *
										psi_j_potential_minus
                                     	) *
                                      	fe_face_values.JxW(q);	
            } // for j

            for(unsigned int j=0; j<dofs_neighbor_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_plus	=
                    fe_neighbor_face_values[VectorField].value(j,q);
                const double 			psi_j_potential_plus		=
                    fe_neighbor_face_values[Potential].value(j,q);

                // int_{face} n^{-} * ( p_{i}^{-} u_{j}^{+} + v^{-} q^{+} ) dx
                // 					  - penalty v^{-}u^{+} dx
                vi_ue_matrix(i,j) += ( 0.5 * (
                                      psi_i_field_minus *
                                      fe_face_values.normal_vector(q) *
                                      psi_j_potential_plus
                                      +
                                      psi_i_potential_minus *
                                      fe_face_values.normal_vector(q) *
                                      psi_j_field_plus )
                                      -
                                      beta *
                                      psi_i_field_minus *
                                      psi_j_potential_plus
                                      +
                                      beta *
                                      psi_i_potential_minus *
                                      psi_j_field_plus
                                      -
                                      penalty *
                            		  psi_i_potential_minus *
                                      psi_j_potential_plus
                                     ) *
                                     fe_face_values.JxW(q);
            } // for j
        } // for i

        for(unsigned int i=0; i<dofs_neighbor_cell; i++)
        {
            const Tensor<1,dim>  psi_i_field_plus =
                					fe_neighbor_face_values[VectorField].value(i,q);
            const double		 psi_i_potential_plus =
             						fe_neighbor_face_values[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_this_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_minus				=
                  						 fe_face_values[VectorField].value(j,q);
                const double 		psi_j_potential_minus		=
                   						 fe_face_values[Potential].value(j,q);

                // int_{face} -n^{-} * ( p_{i}^{+} u_{j}^{-} + v^{+} q^{-} )
                // 					  - penalty v^{+}u^{-} dx
                ve_ui_matrix(i,j) +=	(
                                       -0.5 * (
                                       	psi_i_field_plus *
                                		fe_face_values.normal_vector(q) *
                                        psi_j_potential_minus
                                        +
                                        psi_i_potential_plus *
                                 		fe_face_values.normal_vector(q) *
                                        psi_j_field_minus)
                                        -
                                        beta *
                                        psi_i_field_plus *
                                        psi_j_potential_minus
                                        +
                                        beta *
                                        psi_i_potential_plus *
                                        psi_j_field_minus
                                        -
                                        penalty *
                                        psi_i_potential_plus *
                                        psi_j_potential_minus
                                        ) *
                                    	fe_face_values.JxW(q);
            } // for j

            for(unsigned int j=0; j<dofs_neighbor_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_plus =
                   						 fe_neighbor_face_values[VectorField].value(j,q);
                const double 		psi_j_potential_plus =
                   						 fe_neighbor_face_values[Potential].value(j,q);

                // int_{face} -n^{-} * ( p_{i}^{+} u_{j}^{+} + v^{+} q^{+} )
                // 					  + penalty v^{+}u^{+} dx
                ve_ue_matrix(i,j) +=	(-0.5 * (
										psi_i_field_plus *
										fe_face_values.normal_vector(q) *
										psi_j_potential_plus
                                        +
                                        psi_i_potential_plus *
                                        fe_face_values.normal_vector(q) *
                                        psi_j_field_plus )
                                        +
                                        beta *
                                        psi_i_field_plus *
                                        psi_j_potential_plus
                                        -
                                        beta *
                                        psi_i_potential_plus *
                                        psi_j_field_plus
                                        +
                                        penalty *
                                        psi_i_potential_plus *
                                        psi_j_potential_plus
                                        ) *
                                      	fe_face_values.JxW(q);
            } // for j

        } // for i
    } // for q
} // end assemble_flux_terms()


template<int dim>
void LDGPoissonProblem<dim>::solve()
{
    TimerOutput::Scope t(computing_timer, "solve");

    // solvers need a solution vector with no overlap
    // across the processors
    TrilinosWrappers::MPI::Vector
    completely_distributed_solution(system_rhs);

    // preform solve
    solver.solve(system_matrix,
				completely_distributed_solution,
				system_rhs);

    // distribute the constraints
    constraints.distribute(completely_distributed_solution);

    // copy totally distributed solution vector
    // to solution vector with some overlap between processors
    locally_relevant_solution = completely_distributed_solution;
    //

}

template<int dim>
void
LDGPoissonProblem<dim>::
compute_errors(double & density_error,
							 double & flux_error)
{
	const ComponentSelectFunction<dim> potential_mask(dim, dim+1);
	const ComponentSelectFunction<dim> vectorField_mask(std::make_pair(0,dim), dim+1);
		
	unsigned int degree		= dof_handler.get_fe().degree;
	unsigned int n_cells	= triangulation.n_active_cells();
	
	QTrapez<1>				q_trapez;
	QIterated<dim> 		quadrature(q_trapez, degree+2);
	Vector<double> 		local_errors(n_cells);

	VectorTools::integrate_difference(dof_handler, 
									locally_relevant_solution,
									true_solution, 
									local_errors, 
									quadrature, 
									VectorTools::L2_norm,
									&potential_mask);

	const double total_local_error_1 = local_errors.l2_norm();

	density_error	= std::sqrt(Utilities::MPI::sum(
								total_local_error_1 * total_local_error_1,
								MPI_COMM_WORLD)); 
	
	VectorTools::integrate_difference(dof_handler, 
									locally_relevant_solution, 
									true_solution,
									local_errors, 
									quadrature, 
									VectorTools::L2_norm,
									&vectorField_mask);

	const double total_local_error_2 = local_errors.l2_norm();

	flux_error	= std::sqrt(Utilities::MPI::sum(total_local_error_2 * total_local_error_2,
												MPI_COMM_WORLD)); 	
}


	
template<int dim>
void 
LDGPoissonProblem<dim>::
output_results()	const
{
    std::vector<std::string> solution_names;
    switch(dim)
    {
    case 1:
        solution_names.push_back("E");
        solution_names.push_back("Potential");
        break;

    case 2:
        solution_names.push_back("E_x");
        solution_names.push_back("E_y");
        solution_names.push_back("Potential");
        break;

    case 3:
        solution_names.push_back("E_x");
        solution_names.push_back("E_y");
        solution_names.push_back("E_z");
        solution_names.push_back("Potential");
        break;

    default:
        Assert(false, ExcNotImplemented() );
    }

    DataOut<dim>	data_out;
    data_out.attach_dof_handler(dof_handler);
    data_out.add_data_vector(locally_relevant_solution,
                             solution_names);

    Vector<float>	subdomain(triangulation.n_active_cells());

    for(unsigned int i=0; i<subdomain.size(); i++)
        subdomain(i) = triangulation.locally_owned_subdomain();

    data_out.add_data_vector(subdomain,"subdomain");

    data_out.build_patches();

    const std::string filename = ("solution."	+
                                  Utilities::int_to_string(
								  triangulation.locally_owned_subdomain(),4));

    std::ofstream output((filename + ".vtu").c_str());
    data_out.write_vtu(output);

    if(Utilities::MPI::this_mpi_process(MPI_COMM_WORLD) == 0 )
    {
        std::vector<std::string>	filenames;
        for(unsigned int i=0;
        	i < Utilities::MPI::n_mpi_processes(MPI_COMM_WORLD);
            i++)
        {
            filenames.push_back("solution." +
                                Utilities::int_to_string(i,4) +
                                ".vtu");
        }
        std::ofstream master_output("solution.pvtu");
        data_out.write_pvtu_record(master_output, filenames);
    } // end if
}




template<int dim>
void
LDGPoissonProblem<dim>::
run(ConvergenceTable	& 	error_table)
{
    Grid<dim> grid;
    const unsigned int global_refine = n_refine;
  
		grid.make_test_grid(triangulation, 
												global_refine);
  
    h_max = GridTools::maximal_cell_diameter(triangulation);
    penalty = 1.0/h_max;

    make_dofs_and_allocate_memory();
    assemble_system();
	  solve();
    output_results();

		double density_error, flux_error;

		compute_errors(density_error, flux_error);
		
		error_table.add_value("cells", triangulation.n_global_active_cells());
		error_table.add_value("dofs", dof_handler.n_dofs());
		
		error_table.add_value("u", density_error);
		error_table.add_value("q", flux_error);

}

} // NAMESPACE LDG


///////////////////////////////////////////////////////////////////////////////
// MAIN FUNCTION
///////////////////////////////////////////////////////////////////////////////




