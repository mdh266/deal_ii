#ifndef _H_POISSON_TEST__
#define _H_POISSON_TEST__

#include <deal.II/base/function.h>
#include <deal.II/lac/vector.h>



using namespace dealii;

template<int dim>
class Poisson_TrueSolution : public dealii::Function<dim>
{
public:
    Poisson_TrueSolution() : dealii::Function<dim>(dim+1)
    {}

    virtual void vector_value(const dealii::Point<dim> & p,
                              dealii::Vector<double> &values) const;
};

template <int dim>
void Poisson_TrueSolution<dim>::vector_value(const dealii::Point<dim> &p,
        dealii::Vector<double> &values) const
{
    Assert(values.size() == dim+1,
           dealii::ExcDimensionMismatch(values.size(), dim+1) );

    double pot_value=0.0;
    for(unsigned int i = 0; i < dim; i++)
    {
        values(i) = -3.0*p[i]*p[i];
        pot_value += p[i]*p[i]*p[i];
    }
    values(dim) = pot_value;
}


#endif
