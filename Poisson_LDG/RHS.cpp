#ifndef _RHS_H__
#define _RHS_H__

#include <deal.II/base/function.h>
#include <math.h>

using namespace dealii;

template <int dim>
class RightHandSide : public Function<dim>
{
public:
    RightHandSide() : Function<dim>(1)
    {}

    virtual double value(const Point<dim> &p,
                         const unsigned int component = 0 ) const;
};

template <int dim>
double RightHandSide<dim>::value(const Point<dim> &p,
                                 const unsigned int ) const
{

    /*		double return_value = -dim*M_PI*M_PI;
    		for(int i=0; i<dim; i++)
    			return_value *= sin(M_PI*p[i]);
    		return return_value;
    */
    double return_value = 0.0;
    for(unsigned int i=0; i<dim; i++)
    {
        return_value += -6.0*p(i);
    }
    return return_value;

}

#endif
