#ifndef _GRID_H__
#define _GRID_H__

#include<deal.II/grid/tria.h>
#include<deal.II/grid/tria_accessor.h>
#include<deal.II/grid/tria_iterator.h>
#include<deal.II/grid/grid_generator.h>
#include<deal.II/grid/grid_refinement.h>
#include<deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_tools.h>
#include <iostream>
#include<fstream>

//#include <deal.II/base/parameter_handler.h>


using namespace dealii;

template<int dim>
class Grid
{
public:
    Grid();

    void make_grid(Triangulation<dim> & triangulation,
                   int & global_refine);

    void make_local_refined_grid(Triangulation<dim> & triangulation,
                                 int & global_refine,
                                 int & local_refine);

    void make_Dirichlet_boundaries(Triangulation<dim> & triangulation);

    void make_Neumann_boundaries(Triangulation<dim> & triangulation);

    void print_grid(Triangulation<dim> & triangulation);

private:
    unsigned int Dirichlet;
    unsigned int Neumann;
};


#endif
