#ifndef _BOUNDARY_VALUES_H___
#define _BOUNDARY_VALUES_H___

#include <deal.II/base/function.h>
#include <deal.II/lac/vector.h>

using namespace dealii;

// Dirichlet BC Class publicly derived from Function class (inheritence)
template <int dim>
class DirichletBoundaryValues : public Function<dim>
{
public:
    DirichletBoundaryValues() : Function<dim>() // base class initializer
    {}

    // NOTE: Only base class' function needs to be declared virtual
    // gives value at point, component for vector value functions
    virtual double value(const Point<dim> &p,
                         const unsigned int component = 0) const;

    virtual void value_list(const std::vector<Point<dim> > & points,
                            std::vector<double>		& value_list) const;
};


// Dirichlet BC Value function
template <int dim>
inline
double DirichletBoundaryValues<dim>::value(const Point<dim> &p,
        const unsigned int /*component*/) const
{
    double return_value = 0;
    for(unsigned int i=0; i < dim; i++)
        return_value += p[i]*p[i]*p[i];

    return return_value;
}


template<int dim>
void DirichletBoundaryValues<dim>::value_list(const std::vector<Point<dim> > & points,
        									  std::vector<double>		& value_list) const
{
    Assert(value_list.size() == points.size(),
           ExcDimensionMismatch(value_list.size(), points.size()));

    // Avoids using the virtual function somehow because value is
    // inlined and because of way you call it below?
    for(unsigned int p=0; p<points.size(); p++)
        value_list[p] =	DirichletBoundaryValues<dim>::value(points[p]);

}

// Neumann Boundary Condition Class
template<int dim>
class NeumannBoundaryValues : public Function<dim>
{
public:
    NeumannBoundaryValues() : Function<dim>(dim)
    {}

    virtual void	vector_value(const Point<dim> &p,
                                 Point<dim>  			&values) const;

    virtual void	vector_value_list(const std::vector<Point<dim> > 	&points,
                                      std::vector<Point<dim> > 			&value_list) const;

};


// These have return values of Point<dim> as opposed to Vector<double>
// as in step-8, since the normal vectors are Point<dim> and assembling the
// flux for the Neumann BC we would have Vector<double> * Point<dim>
// which is undefined in deal.II
//

template<int dim>
inline
void NeumannBoundaryValues<dim>::vector_value(const Point<dim> &p,
        Point<dim>  	&values) const
{
    for(unsigned int i=0; i<dim; i++)
        values(i) = -3.0*p[i]*p[i];
}

template<int dim>
void NeumannBoundaryValues<dim>::
vector_value_list(const std::vector<Point<dim>> &points,
                  std::vector<Point<dim> >	  	&value_list) const
{
    Assert(value_list.size() == points.size(),
           ExcDimensionMismatch(value_list.size(), points.size()));

    // Avoids using the virtual function somehow because vector_value
    // is inlined and because of the way you call it below?
    for(unsigned int p=0; p<points.size(); p++)
        NeumannBoundaryValues<dim>::vector_value(points[p],
                value_list[p]);

}

#endif
