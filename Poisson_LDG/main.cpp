#include "PoissonLDG.cpp"

int main()
{
    try {
        using namespace dealii;
        using namespace LDG;

        deallog.depth_console(0);


        double h,
               pot_l2_error = 0,
               vec_field_l2_error = 0;

        int degree = 1;

        std::ofstream prt("error_file2.dat");

        for(unsigned int n_refine=3; n_refine<4; n_refine++)
        {
            LDGPoissonProblem<2> 	Poisson(degree, n_refine);
            Poisson.run_with_errors(h, pot_l2_error,
                                    vec_field_l2_error);

            prt << h << "\t"
                << pot_l2_error << "\t"
                << vec_field_l2_error << "\n";
        }
        prt.close();
    }
    catch (std::exception &exc)
    {
        std::cerr << std::endl << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Exception on processing: " << std::endl
                  << exc.what() << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;

        return 1;
    }
    catch (...)
    {
        std::cerr << std::endl << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        std::cerr << "Unknown exception!" << std::endl
                  << "Aborting!" << std::endl
                  << "----------------------------------------------------"
                  << std::endl;
        return 1;
    }



    return 0;
}
