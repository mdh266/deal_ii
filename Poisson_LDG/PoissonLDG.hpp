///
// Poisson solver using Local Discontinuous Galerkin Method
//
//

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/block_sparse_matrix.h> // for block structuring
#include <deal.II/lac/block_sparsity_pattern.h>
#include <deal.II/lac/block_vector.h> // for block structuring
#include <deal.II/lac/sparse_direct.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_dgq.h> // Lagrange dg fe elements
#include <deal.II/fe/fe_dgp.h> // Legendre dg fe elements
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>

#include <fstream>
#include <iostream>

#include "RHS.cpp"
#include "BoundaryValues.cpp"
#include "Debeye.cpp"
#include "Poisson_Test.cpp"
#include "Grid.cpp"

//#include "Poisson_Solver.hpp"

namespace LDG
{
using namespace dealii;

///////////////////////////////////////////////////////////////////////////////
// LDG POISSON CLASS
///////////////////////////////////////////////////////////////////////////////


template <int dim>
class LDGPoissonProblem
{
public:
    LDGPoissonProblem(const unsigned int degree,
                      const unsigned int n_refine);

    ~LDGPoissonProblem();

    void run();
    void run_with_errors(double & h,
                         double & e_pot,
                         double & e_field);

private:
    void make_dofs_and_allocate_memory();
    void assemble_system();
    void assemble_cell_terms(const FEValues<dim> 	&cell_fe,
                             FullMatrix<double> 	&cell_matrix,
                             Vector<double>				&cell_vector);

    void assemble_Neumann_boundary_terms(const FEFaceValues<dim> 	&face_fe,
                                         FullMatrix<double> 		&local_matrix,
                                         Vector<double>				&local_vector);

    void assemble_Dirichlet_boundary_terms(const FEFaceValues<dim> 	&face_fe,
                                           FullMatrix<double> 		&local_matrix,
                                           Vector<double>			&local_vector);

    void assemble_flux_terms(const FEFaceValuesBase<dim> 		&fe_face_values,
                             const FEFaceValuesBase<dim>		&fe_neighbor_face_values,
                             FullMatrix<double> 				&vi_ui_matrix,
                             FullMatrix<double>					&vi_ue_matrix,
                             FullMatrix<double>					&ve_ui_matrix,
                             FullMatrix<double>					&ve_ue_matrix);

    void output_system() const;
    void solve();
    void compute_errors(double & pot_l2_error,
                        double & vec_field_l2_error);
    void output_results() const;

    const unsigned int degree;
    const unsigned int n_refine;
    double penalty;
    double h_max;
    double h_min;


    Triangulation<dim>		triangulation;
    FESystem<dim>			fe;
    DoFHandler<dim>			dof_handler;

    BlockSparsityPattern				sparsity_pattern;
    BlockSparseMatrix<double>			system_matrix;

    BlockVector<double>					solution;
    BlockVector<double>					system_rhs;

    const RightHandSide<dim>				rhs_function;
    const DirichletBoundaryValues<dim>		Dirichlet_bc_function;
    const NeumannBoundaryValues<dim>		Neumann_bc_function;
    const DebeyeInverse<dim>				debeye_function;
};

}
