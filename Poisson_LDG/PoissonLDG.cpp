#include "PoissonLDG.hpp"

namespace LDG
{
using namespace dealii;

template <int dim>
LDGPoissonProblem<dim>::LDGPoissonProblem(const unsigned int degree,
        const unsigned int n_refine)
    :
    degree(degree),
    n_refine(n_refine),
    fe( FESystem<dim>(FE_DGQ<dim>(degree), dim),		1,
       FE_DGQ<dim>(degree), 							1),
    dof_handler(triangulation),
    rhs_function(),
    Dirichlet_bc_function(),
    Neumann_bc_function(),
    debeye_function()
{
}

template <int dim>
LDGPoissonProblem<dim>::~LDGPoissonProblem()
{
    dof_handler.clear();
}

template <int dim>
void LDGPoissonProblem<dim>::make_dofs_and_allocate_memory()
{

    dof_handler.distribute_dofs(fe);

    // Renumber dofs for [ VectorField, Potential ]^{T} set up
    DoFRenumbering::component_wise(dof_handler);

    std::vector<types::global_dof_index> dofs_per_component(dim+1);
    DoFTools::count_dofs_per_component(dof_handler, dofs_per_component);

    // get number of dofs in vector field components and of potential
    // in each component/dimension of vector field has same number of dofs
    const unsigned int n_vector_field = dim * dofs_per_component[0];
//		std::cout << "n_vector_field = " << n_vector_field << std::endl;

    const unsigned int n_potential = dofs_per_component[1];
//		std::cout << "n_potential = " << n_potential << std::endl;

    std::cout << "Number of active cells : "
              << triangulation.n_active_cells()
              << std::endl
//							<< "Total number of cells: "
//							<< triangulation.n_cells()
//							<< std::endl
              << "h_{max} : "
              << h_max
              << std::endl
              << "Number of degrees of freedom: "
              << dof_handler.n_dofs()
              << " (" << n_vector_field << " + " << n_potential << ")"
              << std::endl;

//		std::cout << "base elements : " << fe.n_base_elements() << std::endl;
//		std::cout << "n_blocks : " << fe.n_blocks() << std::endl;
//		std::cout << "dofs_per_cell : " << fe.dofs_per_cell;

    // allocate memory for [A , B ; B^{T} , 0 ]
    BlockDynamicSparsityPattern dsp(2,2);

    // allocate size for A
    dsp.block(0,0).reinit (n_vector_field,
                           n_vector_field);

    //Allocate size for B^{T}
    dsp.block(1,0).reinit (n_potential,
                           n_vector_field);

    // allocate size for B
    dsp.block(0,1).reinit (n_vector_field,
                           n_potential);

    // allocate size for 0
    dsp.block(1,1).reinit (n_potential,
                           n_potential);

    dsp.collect_sizes();


    // creat the actual sparsity pattern:
    DoFTools::make_flux_sparsity_pattern (dof_handler, dsp);
    sparsity_pattern.copy_from(dsp);

    system_matrix.reinit (sparsity_pattern);

//		std::ofstream output("spartsity_pattern");
//		sparsity_pattern.print_gnuplot(output);
//		output.close();

    // allocate memory for solutions
    solution.reinit (2); // [vector field, potential]
    solution.block(0).reinit (n_vector_field); // VECTOR FIELD
    solution.block(1).reinit (n_potential); // POTENTIAL

    solution.collect_sizes ();

    // memeory for RHS
    system_rhs.reinit (2);
    system_rhs.block(0).reinit (n_vector_field); // DIRICHLET BC
    system_rhs.block(1).reinit (n_potential); // RIGHT HAND SIDE
    system_rhs.collect_sizes ();

}

template <int dim>
void LDGPoissonProblem<dim>::assemble_system()
{
    QGauss<dim>			quadrature_formula(fe.degree+1);
    QGauss<dim-1>		face_quadrature_formula(fe.degree+1);

    const UpdateFlags update_flags 	= update_values
                                      | update_gradients
                                      | update_quadrature_points
                                      | update_JxW_values;

    const UpdateFlags face_update_flags	=	update_values
                                            | update_normal_vectors
                                            | update_quadrature_points
                                            | update_JxW_values;

    FEValues<dim>					fe_values(fe,
            quadrature_formula,
            update_flags);

    FEFaceValues<dim>			fe_face_values(fe,
            face_quadrature_formula,
            face_update_flags);

    FESubfaceValues<dim>	fe_subface_values(fe, face_quadrature_formula,
            face_update_flags);

    FEFaceValues<dim>			fe_neighbor_face_values(fe,
            face_quadrature_formula,
            face_update_flags);

    const unsigned int dofs_per_cell			 = fe.dofs_per_cell;
    const unsigned int n_q_points					 = quadrature_formula.size();
    const unsigned int n_face_q_points		 = face_quadrature_formula.size();


    // Solid integrals boundary conditions and rhs
    FullMatrix<double>		local_matrix(dofs_per_cell, dofs_per_cell);
    Vector<double>				local_vector(dofs_per_cell);

    // Flux integrals
    FullMatrix<double>		vi_ui_matrix(dofs_per_cell, dofs_per_cell);
    FullMatrix<double>		vi_ue_matrix(dofs_per_cell, dofs_per_cell);
    FullMatrix<double>		ve_ui_matrix(dofs_per_cell, dofs_per_cell);
    FullMatrix<double>		ve_ue_matrix(dofs_per_cell, dofs_per_cell);

    std::vector<types::global_dof_index> local_dof_indices(dofs_per_cell);
    std::vector<types::global_dof_index> local_neighbor_dof_indices(dofs_per_cell);

    std::vector<double>							local_debeye_values(n_q_points);
    std::vector<double>							local_rhs_values(n_q_points);
    std::vector<double>							local_bc_values(n_face_q_points);

    typename DoFHandler<dim>::active_cell_iterator
    cell = dof_handler.begin_active(),
    endc = dof_handler.end();

    for(; cell!=endc; cell++)
    {
        local_matrix = 0;
        local_vector = 0;

        fe_values.reinit(cell);

        assemble_cell_terms(fe_values,
                            local_matrix,
                            local_vector);

        // need this before to construct global flux matrices
        cell->get_dof_indices(local_dof_indices);

        // loop over all the faces of this cel/
        for(unsigned int face_no=0; face_no< GeometryInfo<dim>::faces_per_cell; ++face_no)
        {
            typename DoFHandler<dim>::face_iterator 	face = cell->face(face_no);


            // construct matricx for the bounary value flux
            if(face->at_boundary() )
            {
                fe_face_values.reinit(cell, face_no);
                if(face->boundary_id() == 0)
                {
                    assemble_Dirichlet_boundary_terms(fe_face_values,
                                                      local_matrix,
                                                      local_vector);
                }
                else if(face->boundary_id() == 1)
                {
                    assemble_Neumann_boundary_terms(fe_face_values,
                                                    local_matrix,
                                                    local_vector);
                }
                else
                    Assert(false, ExcNotImplemented() );
            }
            else
            {

                // now we are on the interior face elements
                // we want to make sure that neighbor to this state is a valid
                // cell
                Assert(cell->neighbor(face_no).state() == IteratorState::valid,
                       ExcInternalError());

                // get the neighbor cell which shares this face
                typename DoFHandler<dim>::cell_iterator neighbor =
                    cell->neighbor(face_no);

                // if face has children
                // then the neighbor to this cell's face is more refined
                // then it is.
                if(face->has_children())
                {
                    // face such that neighbor(neigh_face_no) = cell(face_no)
                    const unsigned int neighbor_face_no =
                        cell->neighbor_of_neighbor(face_no);


                    // loop over all the subfaces of this face
                    for(unsigned int subface_no=0;
                            subface_no < face->number_of_children();
                            ++subface_no)
                    {

                        // get the refined neighbor cell that matches this face
                        // and subface number
                        typename DoFHandler<dim>::cell_iterator neighbor_child =
                            cell->neighbor_child_on_subface(face_no, subface_no);

                        // parent cant be more than one level above the child
                        Assert(!neighbor_child->has_children(), ExcInternalError());


                        vi_ui_matrix = 0;
                        vi_ue_matrix = 0;
                        ve_ui_matrix = 0;
                        ve_ue_matrix = 0;

                        // reinitialize this fe_values to this cell's subface and
                        // neighbor_childs fe values to the face
                        // NOTE: this uses FESubfaceValues!!!!
                        fe_subface_values.reinit(cell, face_no, subface_no);
                        fe_neighbor_face_values.reinit(neighbor_child, neighbor_face_no);


                        assemble_flux_terms(fe_subface_values,
                                            fe_neighbor_face_values,
                                            vi_ui_matrix,
                                            vi_ue_matrix,
                                            ve_ui_matrix,
                                            ve_ue_matrix);


                        neighbor_child->get_dof_indices(local_neighbor_dof_indices);

                        // can do using constraints?
                        for(unsigned int i=0; i<dofs_per_cell; i++)
                        {
                            for(unsigned int j=0; j<dofs_per_cell; j++)
                            {
                                system_matrix.add(local_dof_indices[i],
                                                  local_dof_indices[j],
                                                  vi_ui_matrix(i,j));

                                system_matrix.add(local_dof_indices[i],
                                                  local_neighbor_dof_indices[j],
                                                  vi_ue_matrix(i,j));

                                system_matrix.add(local_neighbor_dof_indices[i],
                                                  local_dof_indices[j],
                                                  ve_ui_matrix(i,j));

                                system_matrix.add(local_neighbor_dof_indices[i],
                                                  local_neighbor_dof_indices[j],
                                                  ve_ue_matrix(i,j));
                            } // for i
                        } // for j
                    } // for subface_no

                } // if face has children
                else
                {
                    // we know that the neighbor of this cell face is on the same
                    // refinement level and therefore, the
                    // cell with the lower index does the work
                    if(neighbor->level() == cell->level() &&
                            neighbor->index() > cell->index() )
                    {

                        // face such that neighbor(neigh_face_no) = cell(face_no)
                        const unsigned int neighbor_face_no =
                            cell->neighbor_of_neighbor(face_no);

                        vi_ui_matrix = 0;
                        vi_ue_matrix = 0;
                        ve_ui_matrix = 0;
                        ve_ue_matrix = 0;

                        // reinitiatiz the fe face values to be for this cell's face
                        // and neighbors face
                        fe_face_values.reinit(cell, face_no);
                        fe_neighbor_face_values.reinit(neighbor, neighbor_face_no);


                        assemble_flux_terms(fe_face_values,
                                            fe_neighbor_face_values,
                                            vi_ui_matrix,
                                            vi_ue_matrix,
                                            ve_ui_matrix,
                                            ve_ue_matrix);


                        neighbor->get_dof_indices(local_neighbor_dof_indices);

                        // can do using constraints?
                        for(unsigned int i=0; i<dofs_per_cell; i++)
                        {
                            for(unsigned int j=0; j<dofs_per_cell; j++)
                            {
                                system_matrix.add(local_dof_indices[i],
                                                  local_dof_indices[j],
                                                  vi_ui_matrix(i,j));

                                system_matrix.add(local_dof_indices[i],
                                                  local_neighbor_dof_indices[j],
                                                  vi_ue_matrix(i,j));

                                system_matrix.add(local_neighbor_dof_indices[i],
                                                  local_dof_indices[j],
                                                  ve_ui_matrix(i,j));

                                system_matrix.add(local_neighbor_dof_indices[i],
                                                  local_neighbor_dof_indices[j],
                                                  ve_ue_matrix(i,j));
                            } // for i
                        } // for j
                    } // if
                } // else face doesnt have children
            } // else if interior face
        } // end for face_no

        for(unsigned int i=0; i<dofs_per_cell; i++)
            for(unsigned int j=0; j<dofs_per_cell; j++)
                system_matrix.add(local_dof_indices[i],
                                  local_dof_indices[j],
                                  local_matrix(i,j) );

        for(unsigned int i=0; i<dofs_per_cell; i++)
            system_rhs(local_dof_indices[i]) += local_vector[i];

    } // end for cell

} // end assemble system

template<int dim>
void LDGPoissonProblem<dim>::assemble_cell_terms(
    const FEValues<dim> 	&cell_fe,
    FullMatrix<double> 	&cell_matrix,
    Vector<double>				&cell_vector)
{
    const unsigned int dofs_per_cell = cell_fe.dofs_per_cell;
    const unsigned int n_q_points		 = cell_fe.n_quadrature_points;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);

    std::vector<double>							rhs_values(n_q_points);
    std::vector<Tensor<2, dim> >		debeye_values(n_q_points);


    rhs_function.value_list(cell_fe.get_quadrature_points(),
                            rhs_values);

    debeye_function.value_list(cell_fe.get_quadrature_points(),
                               debeye_values);

    for(unsigned int q=0; q<n_q_points; q++)
    {
        for(unsigned int i=0; i<dofs_per_cell; i++)
        {
            const Tensor<1, dim>  psi_i_field		   = cell_fe[VectorField].value(i,q);
            const double 		  div_psi_i_field	   = cell_fe[VectorField].divergence(i,q);
            const double	 	  psi_i_potential	   = cell_fe[Potential].value(i,q);
            const Tensor<1, dim>  grad_psi_i_potential = cell_fe[Potential].gradient(i,q);

            for(unsigned int j=0; j<dofs_per_cell; j++)
            {
                const Tensor<1, dim>	psi_j_field	    = cell_fe[VectorField].value(j,q);
                const double 			psi_j_potential	= cell_fe[Potential].value(j,q);

                cell_matrix(i,j)  +=	( ( debeye_values[q] * psi_i_field * psi_j_field)
                                          -
                                          (div_psi_i_field * psi_j_potential)
                                          -
                                          (grad_psi_i_potential * psi_j_field)
                                     ) * cell_fe.JxW(q);
            } // for j

            // contribution from RHS
            cell_vector(i) += psi_i_potential *
                              rhs_values[q] *
                              cell_fe.JxW(q);

        } // for i
    }	// for q
} // assemble cell


template<int dim>
void LDGPoissonProblem<dim>::assemble_Dirichlet_boundary_terms(
    const FEFaceValues<dim> 	&face_fe,
    FullMatrix<double>				&local_matrix,
    Vector<double>						&local_vector)
{
    const unsigned int dofs_per_cell = face_fe.dofs_per_cell;
    const unsigned int n_q_points		 = face_fe.n_quadrature_points;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);

    std::vector<double>							Dirichlet_bc_values(n_q_points);

    Dirichlet_bc_function.value_list(face_fe.get_quadrature_points(),
                                     Dirichlet_bc_values);

    for(unsigned int q=0; q<n_q_points; q++)
    {
        for(unsigned int i=0; i<dofs_per_cell; i++)
        {
            const Tensor<1, dim>  psi_i_field		= face_fe[VectorField].value(i,q);
            const double 		psi_i_potential		= face_fe[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_per_cell; j++)
            {
                const Tensor<1, dim>  psi_j_field   = face_fe[VectorField].value(j,q);

                // flux for \hat{q}
                // int_{\Gamma_{D}} v^{-} n^{-} q^{-} ds
                local_matrix(i,j) += psi_i_potential * (
                                         face_fe.normal_vector(q) *
                                         psi_j_field
                                     ) *
                                     face_fe.JxW(q);

            } // for j

            /// flux for \hat{u}
            // int_{\Gamma_{D}} -p^{-} n^{-} u_{D} ds
            local_vector(i) += -1.0 * psi_i_field *
                               face_fe.normal_vector(q) *
                               Dirichlet_bc_values[q] *
                               face_fe.JxW(q);

        } // for i
    }	// for q
} // end assemble_Dirichlet_boundary_terms()

template<int dim>
void LDGPoissonProblem<dim>::assemble_Neumann_boundary_terms(
    const FEFaceValues<dim> 	&face_fe,
    FullMatrix<double>				&local_matrix,
    Vector<double>						&local_vector)
{
    const unsigned int dofs_per_cell = face_fe.dofs_per_cell;
    const unsigned int n_q_points		 = face_fe.n_quadrature_points;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);

    const std::vector<Point<dim>>			normals	= face_fe.get_normal_vectors();
    std::vector<Point<dim> >			 		Neumann_bc_values(n_q_points);

    Neumann_bc_function.vector_value_list(face_fe.get_quadrature_points(),
                                          Neumann_bc_values);

    for(unsigned int q=0; q<n_q_points; q++)
    {
        for(unsigned int i=0; i<dofs_per_cell; i++)
        {
            const Tensor<1, dim>  psi_i_field			 = face_fe[VectorField].value(i,q);
            const double 		  psi_i_potential	     = face_fe[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_per_cell; j++)
            {

                const double 	 psi_j_potential		 = face_fe[Potential].value(j,q);

                // flux for \hat{q}
                // int_{\Gamma_{N}} p^{-} n^{-} u^{-} ds
                local_matrix(i,j) += psi_i_field *
                                     face_fe.normal_vector(q) *
                                     psi_j_potential *
                                     face_fe.JxW(q);

            } // for j

            // flux for \hat{u}
            // int_{\Gamma_{N}} -v^{-} n^{-} g_{N} ds
            local_vector(i) += -1.0 * psi_i_potential *
                               (Neumann_bc_values[q] *
                                normals[q]) *
                               face_fe.JxW(q);

        } // for i
    }	// for q

}

template<int dim>
void LDGPoissonProblem<dim>::assemble_flux_terms(
    const FEFaceValuesBase<dim> 	&fe_face_values,
    const FEFaceValuesBase<dim>		&fe_neighbor_face_values,
    FullMatrix<double> 				&vi_ui_matrix,
    FullMatrix<double>				&vi_ue_matrix,
    FullMatrix<double>				&ve_ui_matrix,
    FullMatrix<double>				&ve_ue_matrix)
{
    const unsigned int n_face_points 	=	fe_face_values.n_quadrature_points;
    const unsigned int dofs_this_cell 	= fe_face_values.dofs_per_cell;
    const unsigned int dofs_neighbor_cell =	fe_neighbor_face_values.dofs_per_cell;

    const FEValuesExtractors::Vector VectorField(0);
    const FEValuesExtractors::Scalar Potential(dim);


    const std::vector<double>	&JxW		= fe_face_values.get_JxW_values();
    const std::vector<Point<dim>>	normals	= fe_face_values.get_normal_vectors();

    // beta would be for giving the LDG/ALternating fluxes
    Point<dim> beta;
    for(int i=0; i<dim; i++)
        beta(i) = 1.0;
    beta /= sqrt(beta.square() );

    for(unsigned int q=0; q<n_face_points; q++)
    {
        for(unsigned int i=0; i<dofs_this_cell; i++)
        {
            const Tensor<1,dim>  psi_i_field_minus		 	 =
                fe_face_values[VectorField].value(i,q);
            const double			psi_i_potential_minus	 =
                fe_face_values[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_this_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_minus				=
                    fe_face_values[VectorField].value(j,q);
                const double 			psi_j_potential_minus		=
                    fe_face_values[Potential].value(j,q);

                // int_{face} n^{-} * ( p_{i}^{-} u_{j}^{-} + v^{-} q^{-} ) dx
                // 					  + penalty v^{-}u^{-} dx
                vi_ui_matrix(i,j)	+= (
                                           0.5 * (
                                               psi_i_field_minus *
                                               normals[q] *
                                               psi_j_potential_minus
                                               +
                                               psi_i_potential_minus *
                                               normals[q] *
                                               psi_j_field_minus )
                                           +
                                           beta *
                                           psi_i_field_minus *
                                           psi_j_potential_minus
                                           -
                                           beta *
                                           psi_i_potential_minus *
                                           psi_j_field_minus
                                           +
                                           penalty *
                                           psi_i_potential_minus *
                                           psi_j_potential_minus
                                       ) *
                                       JxW[q];
            } // for j

            for(unsigned int j=0; j<dofs_neighbor_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_plus	=
                    fe_neighbor_face_values[VectorField].value(j,q);
                const double 			psi_j_potential_plus		=
                    fe_neighbor_face_values[Potential].value(j,q);

                // int_{face} n^{-} * ( p_{i}^{-} u_{j}^{+} + v^{-} q^{+} ) dx
                // 					  - penalty v^{-}u^{+} dx
                vi_ue_matrix(i,j) += (
                                         0.5 * (
                                             psi_i_field_minus *
                                             normals[q] *
                                             psi_j_potential_plus
                                             +
                                             psi_i_potential_minus *
                                             normals[q] *
                                             psi_j_field_plus )
                                         -
                                         beta *
                                         psi_i_field_minus *
                                         psi_j_potential_plus
                                         +
                                         beta *
                                         psi_i_potential_minus *
                                         psi_j_field_plus
                                         -
                                         penalty *
                                         psi_i_potential_minus *
                                         psi_j_potential_plus
                                     ) *
                                     JxW[q];
            } // for j
        } // for i

        for(unsigned int i=0; i<dofs_neighbor_cell; i++)
        {
            const Tensor<1,dim>  psi_i_field_plus		 	 =
                fe_neighbor_face_values[VectorField].value(i,q);
            const double			psi_i_potential_plus	 =
                fe_neighbor_face_values[Potential].value(i,q);

            for(unsigned int j=0; j<dofs_this_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_minus				=
                    fe_face_values[VectorField].value(j,q);
                const double 			psi_j_potential_minus		=
                    fe_face_values[Potential].value(j,q);


                ve_ui_matrix(i,j) +=	(
                                            -0.5 * (
                                                psi_i_field_plus *
                                                normals[q]*
                                                psi_j_potential_minus
                                                +
                                                psi_i_potential_plus *
                                                normals[q]*
                                                psi_j_field_minus)
                                            -
                                            beta *
                                            psi_i_field_plus *
                                            psi_j_potential_minus
                                            +
                                            beta *
                                            psi_i_potential_plus *
                                            psi_j_field_minus
                                            -
                                            penalty *
                                            psi_i_potential_plus *
                                            psi_j_potential_minus
                                        ) *
                                        JxW[q];

            } // for j

            for(unsigned int j=0; j<dofs_neighbor_cell; j++)
            {
                const Tensor<1,dim>	psi_j_field_plus	=
                    fe_neighbor_face_values[VectorField].value(j,q);
                const double 			psi_j_potential_plus		=
                    fe_neighbor_face_values[Potential].value(j,q);

                // int_{face} -n^{-} * ( p_{i}^{+} u_{j}^{+} + v^{+} q^{+} )
                // 					  + penalty v^{+}u^{+} dx
                ve_ue_matrix(i,j) +=	(
                                            -0.5 * (
                                                psi_i_field_plus *
                                                normals[q] *
                                                psi_j_potential_plus
                                                +
                                                psi_i_potential_plus *
                                                normals[q] *
                                                psi_j_field_plus )
                                            +
                                            beta *
                                            psi_i_field_plus *
                                            psi_j_potential_plus
                                            -
                                            beta *
                                            psi_i_potential_plus *
                                            psi_j_field_plus
                                            +
                                            penalty *
                                            psi_i_potential_plus *
                                            psi_j_potential_plus
                                        ) *
                                        JxW[q];
            } // for j

        } // for i
    } // for q
} // end assemble_flux_terms()


template<int dim>
void LDGPoissonProblem<dim>::solve()
{
    SparseDirectUMFPACK direct_solver;

//		direct_solver.Hello();

    direct_solver.initialize(system_matrix);
    direct_solver.vmult(solution, system_rhs);
}

template<int dim>
void LDGPoissonProblem<dim>::output_results()	const
{
    std::vector<std::string> solution_names;
    switch(dim)
    {
    case 1:
        solution_names.push_back("E");
        solution_names.push_back("Potential");
        break;

    case 2:
        solution_names.push_back("E_x");
        solution_names.push_back("E_y");
        solution_names.push_back("Potential");
        break;

    case 3:
        solution_names.push_back("E_x");
        solution_names.push_back("E_y");
        solution_names.push_back("E_z");
        solution_names.push_back("Potential");
        break;

    default:
        Assert(false, ExcNotImplemented() );
    }

    DataOut<dim>	data_out;
    data_out.attach_dof_handler(dof_handler);
    data_out.add_data_vector(solution, solution_names);

    data_out.build_patches(0);
    std::ofstream output("solution.vtk");
    data_out.write_vtk(output);


}

template <int dim>
void LDGPoissonProblem<dim>::compute_errors(double & pot_l2_error,
        double & vec_field_l2_error)
{
    const ComponentSelectFunction<dim> potential_mask(dim, dim+1);
    const ComponentSelectFunction<dim>
    vectorField_mask(std::make_pair(0,dim), dim+1);

    Poisson_TrueSolution<dim>	exact_solution;
    Vector<double> cellwise_errors(triangulation.n_active_cells() );

    QTrapez<1>				q_trapez;
    QIterated<dim> 		quadrature(q_trapez, degree+1);


    VectorTools::integrate_difference(dof_handler, solution, exact_solution,
                                      cellwise_errors, quadrature,
                                      VectorTools::L2_norm,
                                      &potential_mask);

    pot_l2_error = cellwise_errors.l2_norm();

    VectorTools::integrate_difference(dof_handler, solution, exact_solution,
                                      cellwise_errors, quadrature,
                                      VectorTools::L2_norm,
                                      &vectorField_mask);

    vec_field_l2_error = cellwise_errors.l2_norm();

    std::cout << "\nErrors: ||e_pot||_L2 = " << pot_l2_error
              << ",		||e_vec_field||_L2 = " << vec_field_l2_error
              << std::endl
              << std::endl;

}

template<int dim>
void LDGPoissonProblem<dim>::output_system() const
{

    std::ofstream output_system("A.mtx");
    system_matrix.print_formatted(output_system);
    output_system.close();

    output_system.open("b.vec");
    system_rhs.print(output_system);
    output_system.close();
}

template <int dim>
void LDGPoissonProblem<dim>::run()
{
    /*		make_grid();
    		make_Dirichlet_boundaries();
    //		make_Neumann_boundaries();
    		make_dofs_and_allocate_memory();
    		assemble_system();
    //		output_system();
    		solve();
    		output_results();
    */
}

template <int dim>
void LDGPoissonProblem<dim>::run_with_errors(double & h,
        double & e_pot,
        double & e_field)
{
    Grid<dim> grid;
    int local = 2;
    int global = n_refine;
//		grid.make_grid(triangulation, global);
    grid.make_local_refined_grid(triangulation, global, local);
    grid.make_Dirichlet_boundaries(triangulation);
    grid.print_grid(triangulation);

    h_max = GridTools::maximal_cell_diameter(triangulation);
    h = h_max;
    penalty = 1.0/h_max;

    //	make_Neumann_boundaries();
    make_dofs_and_allocate_memory();
    assemble_system();
//		output_system();
    solve();
    compute_errors(e_pot, e_field);
    output_results();
}


} // NAMESPACE LDG


///////////////////////////////////////////////////////////////////////////////
// MAIN FUNCTION
///////////////////////////////////////////////////////////////////////////////




