#ifndef _DEBEYE_H__
#define _DEBEYE_H__

#include <deal.II/base/tensor_function.h>


using namespace dealii;

// Inverse K^{-1} Tensor
template<int dim>
class DebeyeInverse : public TensorFunction<2,dim>
{
public:
    DebeyeInverse() : TensorFunction<2,dim>()
    {}

    virtual void value_list(const std::vector<Point<dim> > &points,
                            std::vector<Tensor<2,dim> > &values) const;
};


template <int dim>
void
DebeyeInverse<dim>::value_list(const std::vector<Point<dim> > &points,
                               std::vector<Tensor<2,dim> > &values) const
{
    Assert( points.size() == values.size(),
            ExcDimensionMismatch(points.size(), values.size() ) );

    // returns vector of tensors evaluted at point point[p]
    for(unsigned int p=0; p < points.size(); p++)
    {
        values[p].clear();
        for(unsigned int d=0; d<dim; d++)
        {
            values[p][d][d] = 1.0; // K^{-1}_{dd}(point[p])
        }
    }
}


#endif
