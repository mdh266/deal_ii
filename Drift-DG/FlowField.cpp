#include "FlowField.hpp"

// The flow field is chosen to be a quarter circle with counterclockwise
// flow direction and with the origin as midpoint for the right half of the
// domain with positive $x$ values, whereas the flow simply goes to the left
// in the left part of the domain at a velocity that matches the one coming
// in from the right. In the circular part the magnitude of the flow
// velocity is proportional to the distance from the origin. This is a
// difference to step-12, where the magnitude was 1 everywhere. the new
// definition leads to a linear variation of $\beta$ along each given face
// of a cell. On the other hand, the solution $u(x,y)$ is exactly the same
// as before.
namespace Advection
{
using namespace dealii;

template <int dim>
void Beta<dim>::value_list(const std::vector<Point<dim> > &points,
                           std::vector<Point<dim> > &values) const
{
    Assert(values.size()==points.size(),
           ExcDimensionMismatch(values.size(),points.size()));

    for (unsigned int i=0; i<points.size(); ++i)
    {
        //   if (points[i](0) > 0)
        //  {
        values[i](0) = +1.0;//-points[i](1);
        values[i](1) = +1.0;//points[i](0);
        /*        }
              else
                {
                  values[i] = Point<dim>();
                  values[i](0) = -points[i](1);
                }*/
    }
}

}
