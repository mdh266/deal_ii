#ifndef _ASSEMBLY_H__
#define _ASSEMBLY_H__

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/block_sparse_matrix.h> // for block structuring
#include <deal.II/lac/block_sparsity_pattern.h>
#include <deal.II/lac/block_vector.h> // for block structuring
#include <deal.II/lac/sparse_direct.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_dgq.h> // Lagrange dg fe elements
//#include <deal.II/fe/fe_dgp.h> // Legendre dg fe elements
#include <deal.II/fe/fe_raviart_thomas.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>

#include <fstream>
#include <iostream>


namespace Assembly
{



using namespace dealii;

/////////////////////////////////////////////////////////////////////////////
/// Scratch objects for assembling cell matrices and vectors in parallel
/////////////////////////////////////////////////////////////////////////////

/** This is the scratch object which can be used to assemble the local
* cell matrices and vectors.  We do this so that it can be called by
* Workstreams which will distribute the work amoungst all the cores
* by thread building blocks.
*
*/
template<int dim>
struct AssemblyScratch
{
    AssemblyScratch(const FiniteElement<dim> & fe,
                    const Quadrature<dim>		& quadrature,
                    const Quadrature<dim-1>	& face_quadrature);

    AssemblyScratch(const AssemblyScratch & scratch);

    FEValues<dim>					fe_values;
    FEFaceValues<dim>			fe_face_values;

    std::vector<double>					rhs_values;
    std::vector<double>					bc_values;

};

/////////////////////////////////////////////////////////////////////////////
// COPY DATA
////////////////////////////////////////////////////////////////////////////

// Poisson copy data
template<int dim>
struct CopyData
{
    CopyData(const FiniteElement<dim> & fe);

    CopyData(const CopyData & data);

    Vector<double>												local_rhs;
    FullMatrix<double>										local_matrix;
    std::vector<types::global_dof_index>	local_dof_indices;

};

/////////////////////////////////////////////////////////////////////////////
/// Scratch objects for assembling cell matrices and vectors in parallel
/////////////////////////////////////////////////////////////////////////////

// constructor for the Asssembly Scratch
template<int dim>
AssemblyScratch<dim>::
AssemblyScratch(const FiniteElement<dim> & fe,
                const Quadrature<dim>		& quadrature,
                const Quadrature<dim-1>	& face_quadrature)
    :
    fe_values(fe, quadrature,
             update_values 					 |
             update_gradients 				 |
             update_quadrature_points |
             update_JxW_values				 ),
    fe_face_values(fe, face_quadrature,
                  update_values 					 |
                  update_normal_vectors		 |
                  update_quadrature_points |
                  update_JxW_values				 ),
    rhs_values(quadrature.size()),
    bc_values(face_quadrature.size())
{}

/**  The constructor which you will call if you want to create an
* AssemblyScratch object which will be called when you are looping
* over all the cells by hand and assembling the local conributions
* in a sequential manner.
*/


// copy constructor for the Asssembly Scratch
template<int dim>
AssemblyScratch<dim>::
AssemblyScratch(const AssemblyScratch & scratch)
    :
    fe_values(scratch.fe_values.get_fe(),
             scratch.fe_values.get_quadrature(),
             scratch.fe_values.get_update_flags() ),
    fe_face_values(scratch.fe_face_values.get_fe(),
                  scratch.fe_face_values.get_quadrature(),
                  scratch.fe_face_values.get_update_flags() ),
    rhs_values(scratch.rhs_values),
    bc_values(scratch.bc_values)
{}


// constructor
template<int dim>
CopyData<dim>::
CopyData(const FiniteElement<dim> & fe)
    :
    local_rhs(fe.dofs_per_cell),
    local_matrix(fe.dofs_per_cell,
                fe.dofs_per_cell),
    local_dof_indices(fe.dofs_per_cell)
{ }

// copy constructor
template<int dim>
CopyData<dim>::
CopyData(const CopyData & data)
    :
    local_rhs(data.local_rhs),
    local_matrix(data.local_matrix),
    local_dof_indices(data.local_dof_indices)
{ }


} // end namespace Assembly
#endif
