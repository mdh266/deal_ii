#include "../include/SolarCell.hpp"
#include "Grid.cpp"
#include "Assembly.cpp"
#include "MixedFEM.cpp"
#include "LDG.cpp"
#include "Generation.cpp"
#include "InitialConditions.cpp"
#include "BiasValues.cpp"
#include "PostProcessor.cpp"

namespace SOLARCELL
{
	using namespace dealii;

	template<int dim>
	SolarCellProblem<dim>::
	SolarCellProblem(const unsigned int degree,
									 ParameterHandler & param)
	:
	degree(degree),
	prm(param),
	Poisson_dof_handler(Poisson_triangulation),
	Poisson_fe(FE_RaviartThomas<dim>(degree), 					 1,
						 FE_DGQ<dim>(degree),											 1),
	semiconductor_dof_handler(semiconductor_triangulation),
	electrolyte_dof_handler(electrolyte_triangulation),
	carrier_fe(FESystem<dim>(FE_DGQ<dim>(degree), dim), 1,
													 FE_DGQ<dim>(degree), 1),
	Mixed_Assembler(),
	electrons_e(),
	holes_e(),
	reductants_e(),
	oxidants_e(),
	built_in_bias(),
	applied_bias(),
	bulk_bias(),
	generation()
	{
		// set the parameters
		sim_params.parse_and_scale_parameters(prm);

		// set the charges name, charge sign, and mobility
		electron_hole_pair.carrier_1.set_name("electrons");
		electron_hole_pair.carrier_1.charge_sign = -1.0;
		electron_hole_pair.carrier_1.scaled_mobility =
						sim_params.scaled_electron_mobility;
	
		electron_hole_pair.carrier_2.set_name("holes");
		electron_hole_pair.carrier_2.charge_sign = 1.0;
		electron_hole_pair.carrier_2.scaled_mobility =
						sim_params.scaled_hole_mobility;

		redox_pair.carrier_1.set_name("reductants");
		redox_pair.carrier_1.charge_sign = -1.0;
		redox_pair.carrier_1.scaled_mobility =
						sim_params.scaled_reductant_mobility;

		redox_pair.carrier_2.set_name("oxidants");
		redox_pair.carrier_2.charge_sign = 1.0;
		redox_pair.carrier_2.scaled_mobility =
						sim_params.scaled_oxidant_mobility;

		// set the generation function to be on or off

		if(sim_params.illum_or_dark)
			generation.set_illuminated_params(sim_params);
		else
			generation.set_dark_params();
	}	//SolarCellProblem

	// destructor
	template<int dim>
	SolarCellProblem<dim>::
	~SolarCellProblem()
	{
		Poisson_dof_handler.clear();
		semiconductor_dof_handler.clear();
		electrolyte_dof_handler.clear();

	} // ~SolarCellProblem 

	template<int dim>
	void 
	SolarCellProblem<dim>::
	distribute_dofs()
	{
		// distribute Poisson's dofs
		Poisson_dof_handler.distribute_dofs(Poisson_fe);
		
		// renumber dofs for [Elec field, Potential]^{T} set up
		DoFRenumbering::component_wise(Poisson_dof_handler);

		// distribute the dofs for the electron hole pair
		semiconductor_dof_handler.distribute_dofs(carrier_fe);
		
		// Renumber dofs for [ Current, Density ]^{T} set up
		DoFRenumbering::component_wise(semiconductor_dof_handler);

		// distribute the dofs for the redox pair
		electrolyte_dof_handler.distribute_dofs(carrier_fe);
		
		// Renumber dofs for [ Current, Density ]^{T} set up
		DoFRenumbering::component_wise(electrolyte_dof_handler);

}	


	template<int dim>
	void
	SolarCellProblem<dim>::
	allocate_memory()
	{
		std::cout << "Number of active cells : " 
							<< Poisson_triangulation.n_active_cells()
							<< std::endl
							<< "Total number of cells: " 
							<< Poisson_triangulation.n_cells()
							<< std::endl;

		// allocate memory for the poisson object
		Mixed_Assembler.allocate_memory(Poisson_dof_handler,
																		Poisson_fe,
																		Poisson_object);

		// allocate memory for the electron hole pair
		LDG_Assembler.allocate_memory(semiconductor_dof_handler,
																	electron_hole_pair);

		// allocate memory for the redox pair
		LDG_Assembler.allocate_memory(electrolyte_dof_handler,
																	redox_pair);

	}

////////////////////////////////////////////////////////////////////////////////
// 	MIXED METHOD POISSON EQUATIONS
////////////////////////////////////////////////////////////////////////////////
	template<int dim>
	void 
	SolarCellProblem<dim>::
	assemble_Poisson_matrix()
	{
		// BUILD THE MATRIX
		// [ A  B^{T}	]
		// [ B	0			]
		//
		// where A 		 = \int_{omega} p * D dx 
		// 			 B^{T} = \int_{omega} div(p) \Phi dx
		// 			 B		 = \int_{omega} lambda * v div(D) dx
		//
		// Using the mixed method assembler object

		
		// this is shown in deal.II on shared memory parallelism 
		// it builds the above matrix by distributing over multi threads locally
		// and then building up the global matrix sequentially (kinda)

		WorkStream::run(Poisson_dof_handler.begin_active(),
										Poisson_dof_handler.end(),
										std_cxx11::bind(&MixedPoisson::MixedFEM<dim>::
																		assemble_local_Poisson_matrix, 
																		Mixed_Assembler, // this object object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3,
																		sim_params.semiconductor_permittivity,
																		sim_params.electrolyte_permittivity,
																		sim_params.scaled_debeye_length), 
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_Poisson_matrix,
																		this, // this object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::Poisson::CopyData<dim>(Poisson_fe)
										);

	} // end assemble_Poisson_matrix


	template <int dim>
	void 
	SolarCellProblem<dim>::
	copy_local_to_global_Poisson_matrix(
											const Assembly::Poisson::CopyData<dim> & data)
	{
		// distribute local matrix to global Poisson matrix
		Poisson_object.constraints.distribute_local_to_global(data.local_matrix,
																									data.local_dof_indices,
																									Poisson_object.system_matrix);
	}	// copy_local_to_global_poisson

	template <int dim>
	void 
	SolarCellProblem<dim>::
	assemble_test_Poisson_rhs()
	{
		
		///////////////////////////////////////////////////////////////
		// This is the test one which uncoupled 
		///////////////////////////////////////////////////////////////
		WorkStream::run(Poisson_dof_handler.begin_active(),
										Poisson_dof_handler.end(),
										std_cxx11::bind(&MixedPoisson::MixedFEM<dim>::
																		assemble_local_test_rhs,
																		Mixed_Assembler, // the object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_Poisson_rhs,
																		this, // this object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::Poisson::CopyData<dim>(Poisson_fe)
										);
	} // assemble_test_rhs()


	template <int dim>
	void 
	SolarCellProblem<dim>::
	copy_local_to_global_Poisson_rhs(
											const Assembly::Poisson::CopyData<dim> & data)
	{
		// copy the local RHS into the global RHS for Poisson
		Poisson_object.constraints.distribute_local_to_global(data.local_rhs,
																									 data.local_dof_indices,
																									 Poisson_object.system_rhs);
	}	


	template <int dim>
	void
	SolarCellProblem<dim>::
	assemble_Poisson_rhs()
	{
		// reset rhs to zero
		Poisson_object.system_rhs = 0;

		///////////////////////////////////////////////////////////////
		// This is the one coupled to the semiconductor
		///////////////////////////////////////////////////////////////
		WorkStream::run(semiconductor_dof_handler.begin_active(),
										semiconductor_dof_handler.end(),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		assemble_local_Poisson_rhs_for_semiconductor,
																		this, // this object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_Poisson_rhs,
																		this, // this object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::Poisson::CopyData<dim>(Poisson_fe)
										);


		///////////////////////////////////////////////////////////////
		// This is the one coupled to the electrolyte
		///////////////////////////////////////////////////////////////
		WorkStream::run(electrolyte_dof_handler.begin_active(),
										electrolyte_dof_handler.end(),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		assemble_local_Poisson_rhs_for_electrolyte,
																		this, // this object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_Poisson_rhs,
																		this, // this object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::Poisson::CopyData<dim>(Poisson_fe)
										);

	}

	template <int dim>
	void 
	SolarCellProblem<dim>::
	assemble_local_Poisson_rhs_for_semiconductor(
				const typename DoFHandler<dim>::active_cell_iterator 	& cell,
				Assembly::AssemblyScratch<dim>											  & scratch,
				Assembly::Poisson::CopyData<dim>											& data)
	{
		///////////////////////////////////////////////////////////////
		// This is the one coupled to the drift-diffusion equations
		///////////////////////////////////////////////////////////////
		
		const unsigned int	dofs_per_cell 	=	
												scratch.Poisson_fe_values.dofs_per_cell;

		const unsigned int 	n_q_points 			= 
												scratch.Poisson_fe_values.n_quadrature_points;

		const unsigned int	n_face_q_points =	
												scratch.Poisson_fe_face_values.n_quadrature_points;


		// Get the actual values for vector field and potential from FEValues
		// Use Extractors instead of having to deal with shapefunctions directly
		const FEValuesExtractors::Vector VectorField(0); // Vector as in Vector field
		const FEValuesExtractors::Scalar Potential(dim);

		const FEValuesExtractors::Scalar Density(dim);


		// reset the local_rhs vector to be zero
		data.local_rhs=0;

		typename DoFHandler<dim>::active_cell_iterator
								Poisson_cell(&semiconductor_triangulation,
													cell->level(),
													cell->index(),
													&Poisson_dof_handler);

		Poisson_cell->get_dof_indices(data.local_dof_indices);

		// reinitialize the fe_Values on this cell
		scratch.Poisson_fe_values.reinit(Poisson_cell);
		// get the test rhs for poisson
		// Assemble the right hand side on semiconductor side

		scratch.carrier_fe_values.reinit(cell);
			
		// get the electron density values at the previous time step
		scratch.carrier_fe_values[Density].get_function_values(
																				electron_hole_pair.carrier_1.old_solution,
																				scratch.old_carrier_1_density_values);

		// get the hole density values at the previous time step
		scratch.carrier_fe_values[Density].get_function_values(
																				electron_hole_pair.carrier_2.old_solution,
																				scratch.old_carrier_2_density_values);
	

		// get doping profiles values on this cell
		electrons_e.value_list(scratch.carrier_fe_values.get_quadrature_points(),
													scratch.donor_doping_values,
													dim); // calls the density values of the donor profile

		holes_e.value_list(scratch.carrier_fe_values.get_quadrature_points(),
											scratch.acceptor_doping_values,
											dim); // calls the density values of the donor profile

		// Loop over all the quadrature points in this cell
		for(unsigned int q=0; q<n_q_points; q++)
		{
			// loop over the test function dofs for this cell
			for(unsigned int i=0; i<dofs_per_cell; i++)
			{
				// i-th potential basis functions at the point q
				const double psi_i_potential = 
										scratch.Poisson_fe_values[Potential].value(i,q);
		
				// get the local RHS values for this cell
				// = -int_{Omega_{e}} (1/lambda^{2}) v	(N_{D} - N_{A}) - (n - p) dx
				data.local_rhs(i) += -psi_i_potential *
															(
															 (scratch.donor_doping_values[q] 
													 			- 
														 		scratch.acceptor_doping_values[q])
												    	 	+
															 (electron_hole_pair.carrier_1.charge_sign *
																scratch.old_carrier_1_density_values[q]	
																+
																electron_hole_pair.carrier_2.charge_sign *
																scratch.old_carrier_2_density_values[q])
														  ) * scratch.Poisson_fe_values.JxW(q);
		
			} // for i
		} // for q

		// loop over all the faces of this cell to calculate the vector
		// from the dirichlet boundary conditions if the face is on the 
		// Dirichlet portion of the boundary
		for(unsigned int face_no=0;
				face_no<GeometryInfo<dim>::faces_per_cell;
				face_no++)
		{
			// obtain the face_no-th face of this cell
			typename DoFHandler<dim>::face_iterator 	face = Poisson_cell->face(face_no);

			// apply Dirichlet boundary conditions.. 
			// Since we are in a semicondcutor cell we know to apply the 
			// biases
			if((face->at_boundary()) && (face->boundary_id() == Dirichlet))
			{	
				// get the values of the shape functions at this boundary face
				scratch.Poisson_fe_face_values.reinit(Poisson_cell,face_no);
			
				// get the values of the dirichlet boundary conditions evaluated
				// on the quadrature points of this face
				built_in_bias.value_list(
														scratch.Poisson_fe_face_values.get_quadrature_points(),
														scratch.Poisson_bi_values);

				applied_bias.value_list(
														scratch.Poisson_fe_face_values.get_quadrature_points(),
														scratch.Poisson_bc_values);

				// loop over all the quadrature points of this face
				for(unsigned int q=0; q<n_face_q_points; q++)
				{
					// loop over all the test function dofs of this face
					for(unsigned int i=0; i<dofs_per_cell; i++)
					{
						// TODO: DIVIDE BY THERMAL VOLTAGE????

						// - \int_{face} p * n * (phi_{Dichlet}) dx
						data.local_rhs(i)	+=	
													-(scratch.Poisson_fe_face_values[VectorField].value(i,q) *
														scratch.Poisson_fe_face_values.normal_vector(q) *
														(scratch.Poisson_bc_values[q] 
														 +
														 scratch.Poisson_bi_values[q]) *
														scratch.Poisson_fe_face_values.JxW(q));
						} // for i
					} // for q
				} // end if
			} // end for face_no
	} // assemble_local_Poisson_rhs_for_electrolyte

	template <int dim>
	void 
	SolarCellProblem<dim>::
	assemble_local_Poisson_rhs_for_electrolyte(
				const typename DoFHandler<dim>::active_cell_iterator 	& cell,
				Assembly::AssemblyScratch<dim>											  & scratch,
				Assembly::Poisson::CopyData<dim>											& data)	
	{
		///////////////////////////////////////////////////////////////
		// This is the one coupled to the drift-diffusion equations
		///////////////////////////////////////////////////////////////
		
		const unsigned int	dofs_per_cell 	=	
												scratch.Poisson_fe_values.dofs_per_cell;

		const unsigned int 	n_q_points 			= 
												scratch.Poisson_fe_values.n_quadrature_points;

		const unsigned int	n_face_q_points =	
												scratch.Poisson_fe_face_values.n_quadrature_points;

		// Get the actual values for vector field and potential from FEValues
		// Use Extractors instead of having to deal with shapefunctions directly
		const FEValuesExtractors::Vector VectorField(0); // Vector as in Vector field
		const FEValuesExtractors::Scalar Potential(dim);

		const FEValuesExtractors::Scalar Density(dim);


		// reset the local_rhs vector to be zero
		data.local_rhs=0;

		typename DoFHandler<dim>::active_cell_iterator
								Poisson_cell(&electrolyte_triangulation,
													cell->level(),
													cell->index(),
													&Poisson_dof_handler);

		Poisson_cell->get_dof_indices(data.local_dof_indices);

		// reinitialize the fe_Values on this cell
		scratch.Poisson_fe_values.reinit(Poisson_cell);
		// get the test rhs for poisson
		// Assemble the right hand side on semiconductor side

		scratch.carrier_fe_values.reinit(cell);

		// get the reductant density values at the previous time step
		scratch.carrier_fe_values[Density].get_function_values(
																				redox_pair.carrier_1.old_solution,
																				scratch.old_carrier_1_density_values);

		// get the oxidant density values at the previous time step
		scratch.carrier_fe_values[Density].get_function_values(
																				redox_pair.carrier_2.old_solution,
																				scratch.old_carrier_2_density_values);
	
		// get background charge profile values on this cell
//		reductants_e.value_list(scratch.carrier_fe_values.get_quadrature_points(),
//													scratch.donor_doping_values,
//													dim); // calls the density values of the donor profile

//		oxidants_e.value_list(scratch.carrier_fe_values.get_quadrature_points(),
//											scratch.acceptor_doping_values,
//											dim); // calls the density values of the donor profile


		// Loop over all the quadrature points in this cell
		for(unsigned int q=0; q<n_q_points; q++)
		{
			// loop over the test function dofs for this cell
			for(unsigned int i=0; i<dofs_per_cell; i++)
			{
				// i-th potential basis functions at the point q
				const double psi_i_potential = 
										scratch.Poisson_fe_values[Potential].value(i,q);
		
				// get the local RHS values for this cell
				// = - 1/\lambda^{2} \int v (-\rho_{r} + \rho_{o} )dx
				data.local_rhs(i) += -psi_i_potential * 
																(
//															 (scratch.donor_doping_values[q] 
//													 			- 
//														 		scratch.acceptor_doping_values[q])
//												    	 	+
															(redox_pair.carrier_1.charge_sign *
																scratch.old_carrier_1_density_values[q]	
																+
												    	 	redox_pair.carrier_2.charge_sign *
																scratch.old_carrier_2_density_values[q])
														  ) * scratch.Poisson_fe_values.JxW(q);
			} // for i
		} // for q
	// No need to apply Dirichlet boundary conditions as the potential is zero
		
		// loop over all the faces of this cell to calculate the vector
		// from the dirichlet boundary conditions if the face is on the 
		// Dirichlet portion of the boundary
		for(unsigned int face_no=0;
				face_no<GeometryInfo<dim>::faces_per_cell;
				face_no++)
		{
			// obtain the face_no-th face of this cell
			typename DoFHandler<dim>::face_iterator 	face = Poisson_cell->face(face_no);

			// apply Dirichlet boundary conditions.. 
			// Since we are in a semicondcutor cell we know to apply the 
			// biases
			if((face->at_boundary()) && (face->boundary_id() == Dirichlet))
			{	
				// get the values of the shape functions at this boundary face
				scratch.Poisson_fe_face_values.reinit(Poisson_cell,face_no);
			
				// get the values of the dirichlet boundary conditions evaluated
				// on the quadrature points of this face
				bulk_bias.value_list(
														scratch.Poisson_fe_face_values.get_quadrature_points(),
														scratch.Poisson_bc_values);

				// loop over all the quadrature points of this face
				for(unsigned int q=0; q<n_face_q_points; q++)
				{
					// loop over all the test function dofs of this face
					for(unsigned int i=0; i<dofs_per_cell; i++)
					{
						// TODO: DIVIDE BY THERMAL VOLTAGE????

						// - \int_{face} p * n * (phi_{Dichlet}) dx
						data.local_rhs(i)	+=	
													- scratch.Poisson_fe_face_values[VectorField].value(i,q) *
														scratch.Poisson_fe_face_values.normal_vector(q) *
														scratch.Poisson_bc_values[q] *
														scratch.Poisson_fe_face_values.JxW(q);
						} // for i
					} // for q
				} // end if
			} // end for face_no

	} // end assemble_local_Poisson_rhs_for_electrolyte


/////////////////////////////////////////////////////////////////////////////////
// LDG FOR DRIFT-DIFFUSION EQUATIONS
/////////////////////////////////////////////////////////////////////////////////

	template <int dim>
	void 
	SolarCellProblem<dim>::
	assemble_mass_matrix()
	{	
		// assemble mass matrix for the electron_hole pair
		WorkStream::run(semiconductor_dof_handler.begin_active(),
										semiconductor_dof_handler.end(),
										std_cxx11::bind(&LDG_System::LDG<dim>::
																		assemble_local_LDG_mass_matrix,
																		LDG_Assembler, // the Assembler object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3,
																		delta_t),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_semiconductor_mass_matrix,
																		this, 			// tthis object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::DriftDiffusion::CopyData<dim>(carrier_fe)
										);

		// assemble the mass matrix for redox pair
		WorkStream::run(electrolyte_dof_handler.begin_active(),
										electrolyte_dof_handler.end(),
										std_cxx11::bind(&LDG_System::LDG<dim>::
																		assemble_local_LDG_mass_matrix,
																		LDG_Assembler, // the Assembler object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3,
																		delta_t),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_electrolyte_mass_matrix,
																		this, 			// tthis object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::DriftDiffusion::CopyData<dim>(carrier_fe)
										);

	
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	copy_local_to_global_semiconductor_mass_matrix(
				const Assembly::DriftDiffusion::CopyData<dim>			& data)
	{
		// copy local mass matrix into the global mass matrix.. 
		electron_hole_pair.constraints.distribute_local_to_global(
																					data.local_mass_matrix,
																					data.local_dof_indices,
																					electron_hole_pair.mass_matrix);
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	copy_local_to_global_electrolyte_mass_matrix(
				const Assembly::DriftDiffusion::CopyData<dim>			& data)
	{
		// copy local mass matrix into the global mass matrix.. 
		redox_pair.constraints.distribute_local_to_global(
																					data.local_mass_matrix,
																					data.local_dof_indices,
																					redox_pair.mass_matrix);
	}

	template <int dim>
	void 
	SolarCellProblem<dim>::
	assemble_LDG_cell_and_bc_terms(const double & transient_or_steady)
	{	
		// assemble mass matrix for the electron_hole pair
		WorkStream::run(semiconductor_dof_handler.begin_active(),
										semiconductor_dof_handler.end(),
										std_cxx11::bind(&LDG_System::LDG<dim>::
																		assemble_local_LDG_cell_and_bc_terms,
																		LDG_Assembler, // the Assembler object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3,
																		electron_hole_pair.carrier_1.scaled_mobility,
																		electron_hole_pair.carrier_2.scaled_mobility,
																		delta_t,
																		transient_or_steady),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_semiconductor_system_matrix,
																		this, 			// tthis object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::DriftDiffusion::CopyData<dim>(carrier_fe)
										);

		// assemble the mass matrix for redox pair
		WorkStream::run(electrolyte_dof_handler.begin_active(),
										electrolyte_dof_handler.end(),
										std_cxx11::bind(&LDG_System::LDG<dim>::
																		assemble_local_LDG_cell_and_bc_terms,
																		LDG_Assembler, // the Assembler object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3,
																		redox_pair.carrier_1.scaled_mobility,
																		redox_pair.carrier_2.scaled_mobility,
																		delta_t,
																		transient_or_steady),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_electrolyte_system_matrix,
																		this, 			// tthis object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::DriftDiffusion::CopyData<dim>(carrier_fe)
										);
	}


	template<int dim>
	void
	SolarCellProblem<dim>::
	copy_local_to_global_semiconductor_system_matrix(
				const Assembly::DriftDiffusion::CopyData<dim>			& data)
	{
		// copy local system matrix into the global system matrix.. 
		electron_hole_pair.constraints.distribute_local_to_global(
																			data.local_matrix_1,
																			data.local_dof_indices,
																			electron_hole_pair.carrier_1.system_matrix);

		// copy local system matrix into the global system matrix.. 
		electron_hole_pair.constraints.distribute_local_to_global(
																			data.local_matrix_2,
																			data.local_dof_indices,
																			electron_hole_pair.carrier_2.system_matrix);
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	copy_local_to_global_electrolyte_system_matrix(
				const Assembly::DriftDiffusion::CopyData<dim>			& data)
	{
		// copy local system matrix into the global system matrix.. 
		redox_pair.constraints.distribute_local_to_global(
																		data.local_matrix_1,
																		data.local_dof_indices,
																		redox_pair.carrier_1.system_matrix);

		// copy local syste, matrix into the global system matrix.. 
		redox_pair.constraints.distribute_local_to_global(
																		data.local_matrix_2,
																		data.local_dof_indices,
																		redox_pair.carrier_2.system_matrix);
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	copy_local_to_global_semiconductor_system_rhs(						
						const Assembly::DriftDiffusion::CopyData<dim>			& data)
	{
		// copy local system rhs into the global system rhs.. 
		electron_hole_pair.constraints.distribute_local_to_global(
																		data.local_carrier_1_rhs,
																		data.local_dof_indices,
																		electron_hole_pair.carrier_1.system_rhs);

		// copy local syste, matrix into the global system matrix.. 
		electron_hole_pair.constraints.distribute_local_to_global(
																		data.local_carrier_2_rhs,
																		data.local_dof_indices,
																		electron_hole_pair.carrier_2.system_rhs);
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	copy_local_to_global_electrolyte_system_rhs(						
				const Assembly::DriftDiffusion::CopyData<dim>			& data)
	{
		// copy local system rhs into the global system rhs.. 
		redox_pair.constraints.distribute_local_to_global(
																		data.local_carrier_1_rhs,
																		data.local_dof_indices,
																		redox_pair.carrier_1.system_rhs);

		// copy local syste, matrix into the global system matrix.. 
		redox_pair.constraints.distribute_local_to_global(
																		data.local_carrier_2_rhs,
																		data.local_dof_indices,
																		redox_pair.carrier_2.system_rhs);
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	assemble_test_LDG_rhs()
	{
		///////////////////////////////////////////////////////////////////////
		// Test case for the uncoupled version
		//////////////////////////////////////////////////////////////////////	
		
		WorkStream::run(semiconductor_dof_handler.begin_active(),
										semiconductor_dof_handler.end(),
										std_cxx11::bind(&LDG_System::LDG<dim>::
																		assemble_local_test_rhs,
																		LDG_Assembler, // Assembler object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_semiconductor_system_rhs,
																		this, // this object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::DriftDiffusion::CopyData<dim>(carrier_fe)
										);
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	assemble_semiconductor_rhs()
	{
		// set carrier_system_rhs = M * u^{n-1}
		electron_hole_pair.mass_matrix.vmult(electron_hole_pair.carrier_1.system_rhs,
																				 electron_hole_pair.carrier_1.old_solution);

		electron_hole_pair.mass_matrix.vmult(electron_hole_pair.carrier_2.system_rhs,
																				 electron_hole_pair.carrier_2.old_solution);

		// now run through the semiconductor cells and assemble rhs for the 
		// LDG equations.
		WorkStream::run(semiconductor_dof_handler.begin_active(),
										semiconductor_dof_handler.end(),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		assemble_local_semiconductor_rhs,
																		this, // Assembler object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_semiconductor_system_rhs,
																		this, // this object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::DriftDiffusion::CopyData<dim>(carrier_fe)
										);

	}	

	template<int dim>
	void 
	SolarCellProblem<dim>::
	assemble_local_semiconductor_rhs(
								const typename DoFHandler<dim>::active_cell_iterator & cell,
								Assembly::AssemblyScratch<dim>											 & scratch,
								Assembly::DriftDiffusion::CopyData<dim>							 & data)									
	{
		///////////////////////////////////////////////////////////////////////
		// This is for the case of a coupled Poisson's equation and
		// reactive interface
		//////////////////////////////////////////////////////////////////////

		// this assembles the drift term in the ldg formulation.  it uses the 
		// electric field at the current iteration and the density of the 
		// carrier at the previous time step
		const unsigned int dofs_per_cell			 = 
															scratch.carrier_fe_values.dofs_per_cell;
		const unsigned int n_q_points					 = 
																scratch.carrier_fe_values.n_quadrature_points;
		const unsigned int n_face_q_points		 = 
																scratch.carrier_fe_face_values.n_quadrature_points;

		cell->get_dof_indices(data.local_dof_indices);


		// need to get the cell for poisson triangulation which corresponds 
		// to this cell
		typename DoFHandler<dim>::active_cell_iterator
							Poisson_cell(&semiconductor_triangulation,
													cell->level(),
													cell->index(),
													&Poisson_dof_handler);

		// reinitialize the fe_values 
		scratch.carrier_fe_values.reinit(cell);
		scratch.Poisson_fe_values.reinit(Poisson_cell);
		
		// reset the local_rhs to be zero
		data.local_carrier_1_rhs=0;
		data.local_carrier_2_rhs=0;
	
		const FEValuesExtractors::Vector Current(0);
		const FEValuesExtractors::Scalar Density(dim);

		const FEValuesExtractors::Vector ElectricField(0);

		// get the values of carrier_1 and carrier_2 densities at the pevious time step
		scratch.carrier_fe_values[Density].get_function_values(
																				electron_hole_pair.carrier_1.old_solution,
																				scratch.old_carrier_1_density_values);

		scratch.carrier_fe_values[Density].get_function_values(
																				electron_hole_pair.carrier_2.old_solution,
																				scratch.old_carrier_2_density_values);

		generation.value_list(scratch.carrier_fe_values.get_quadrature_points(),
													scratch.generation_values);

		// get the electric field values at the previous time step
		scratch.Poisson_fe_values[ElectricField].get_function_values(
																									Poisson_object.solution,
																									scratch.electric_field_values);

		const double inverse_perm			=		1.0/sim_params.semiconductor_permittivity;

		// loop over all the quadrature points in this cell and compute body integrals
		for(unsigned int q=0; q<n_q_points; q++)
		{
			// loop over all the test function dofs and get the test functions
			for(unsigned int i=0; i<dofs_per_cell; i++)
			{
				const double					psi_i_density	= 
																	scratch.carrier_fe_values[Density].value(i,q);

				const Tensor<1,dim>		psi_i_field		=
																	scratch.carrier_fe_values[Current].value(i,q);
		
				// contribution from RHS function + Drift
				// int_{Omega} v * R dx
				data.local_carrier_1_rhs(i) += ( 
													(psi_i_density * scratch.generation_values[q])
													+
													(psi_i_density *
													SRH_Recombination(
															scratch.old_carrier_1_density_values[q],
															scratch.old_carrier_2_density_values[q],
															sim_params))
													+
														electron_hole_pair.carrier_1.charge_sign *
														psi_i_field *
														inverse_perm *
														scratch.electric_field_values[q] *
														scratch.old_carrier_1_density_values[q]
														) *
														scratch.carrier_fe_values.JxW(q);

				data.local_carrier_2_rhs(i) += ( 
													(psi_i_density * scratch.generation_values[q])
													+
													(psi_i_density *
													SRH_Recombination(
															scratch.old_carrier_1_density_values[q],
															scratch.old_carrier_2_density_values[q],
															sim_params))
														+
														electron_hole_pair.carrier_2.charge_sign *
														psi_i_field *
														inverse_perm *	
														scratch.electric_field_values[q] *
														scratch.old_carrier_2_density_values[q]
														) *
														scratch.carrier_fe_values.JxW(q);

			} // for i
		}	// for q
		
		// loop over all the faces of this cell and compute the contribution 
		// from the boundary conditions
		for(unsigned int face_no=0; 
				face_no< GeometryInfo<dim>::faces_per_cell;
				face_no++)
		{
			// get the face_no-th face of this cell
			typename DoFHandler<dim>::face_iterator 	face = cell->face(face_no);
			
			// if on boundary apply boundayr conditions	
			if(face->at_boundary() )
			{
				// reinitialize the fe_face_values for this cell ONLY if it is as the
				//boundary otherwise its a waste.  then assemble the appropriate
				//boundary conditions
				scratch.carrier_fe_face_values.reinit(cell, face_no);
			
				if(face->boundary_id() == Dirichlet)
				{
					// Get the doping profile values for the boundary conditions
					electrons_e.value_list(
														scratch.carrier_fe_face_values.get_quadrature_points(),
														scratch.carrier_1_bc_values,
														dim); // calls the density values of the donor profile
																	// not the current ones
					holes_e.value_list(
													scratch.carrier_fe_face_values.get_quadrature_points(),
													scratch.carrier_2_bc_values,
													dim); // calls the density values of the donor profile
																// not the current ones

					// loop over all the quadrature points on this face
					for(unsigned int q=0; q<n_face_q_points; q++)
					{
						// loop over all the test function dofs on this face
						for(unsigned int i=0; i<dofs_per_cell; i++)
						{
							// get the test function
							const Tensor<1, dim>  psi_i_field	= 
													scratch.carrier_fe_face_values[Current].value(i,q);
 
							// int_{\Gamma_{D}} -p^{-} n^{-} u_{D} ds
							data.local_carrier_1_rhs(i) += 
																-1.0 * psi_i_field *
													 			 scratch.carrier_fe_face_values.normal_vector(q) *
																 scratch.carrier_1_bc_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q);				
					
							data.local_carrier_2_rhs(i) +=  
																-1.0 * psi_i_field *
													 			 scratch.carrier_fe_face_values.normal_vector(q) *
	  														  scratch.carrier_2_bc_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q);				


						} // for i
					}	// for q
				} // end Dirichlet
				else if(face->boundary_id() == Interface)
				{
					// Get the doping profile values for the boundary conditions
					electrons_e.value_list(
														scratch.carrier_fe_face_values.get_quadrature_points(),
														scratch.carrier_1_bc_values,
														dim); // calls the density values of the donor profile
																	// not the current ones
					holes_e.value_list(
													scratch.carrier_fe_face_values.get_quadrature_points(),
													scratch.carrier_2_bc_values,
													dim); // calls the density values of the donor profile
																// not the current ones
					
	
					// get the electrona and hole densities at the pevious time step
					scratch.carrier_fe_face_values[Density].get_function_values(
																				electron_hole_pair.carrier_1.old_solution,
																				scratch.electron_interface_values);

					scratch.carrier_fe_face_values[Density].get_function_values(
																				electron_hole_pair.carrier_2.old_solution,
																				scratch.hole_interface_values);

					// get the electrona and hole densities at the pevious time step
					scratch.carrier_fe_face_values[Density].get_function_values(
																				redox_pair.carrier_1.old_solution,
																				scratch.reductant_interface_values);

					scratch.carrier_fe_face_values[Density].get_function_values(
																				redox_pair.carrier_2.old_solution,
																				scratch.oxidant_interface_values);
		
					// loop over all the quadrature points on this face
					for(unsigned int q=0; q<n_face_q_points; q++)
					{
						// loop over all the test function dofs on this face
						for(unsigned int i=0; i<dofs_per_cell; i++)
						{
							// get the test function
							const double psi_i_density =
											scratch.carrier_fe_face_values[Density].value(i,q);
	
							// int_{\Gamma_{D}} -v^{-} ket (rho_n - rho_n^e) rho_o ds
							data.local_carrier_1_rhs(i) += 
																-1.0 * psi_i_density *
																 sim_params.scaled_k_et *
																 (scratch.electron_interface_values[q]
																	-
																	scratch.carrier_1_bc_values[q]) * 
																 scratch.oxidant_interface_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q);				
					
							// int_{\Gamma_{D}}  v^{-} kht (rho_p - rho_p^e) rho_r ds
							data.local_carrier_2_rhs(i) +=  
																	+1.0 * psi_i_density *
																 sim_params.scaled_k_ht *
																 (scratch.hole_interface_values[q]
																	-
																	scratch.carrier_2_bc_values[q]) *
																 scratch.reductant_interface_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q);				
		
						} // for i
					} // for j

				}
				else if(face->boundary_id() == Neumann)
				{
					// NOTHIN TO DO IF INSULATING
				}
				else
					Assert(false, ExcNotImplemented() );
			} // end at boundary
		} // end for face_no
	
	} // end assemble_local_semiconductor_rhs

	template<int dim>
	void
	SolarCellProblem<dim>::
	assemble_electrolyte_rhs()
	{
		// set carrier_system_rhs = M * u^{n-1}
		redox_pair.mass_matrix.vmult(redox_pair.carrier_1.system_rhs,
																 redox_pair.carrier_1.old_solution);

		redox_pair.mass_matrix.vmult(redox_pair.carrier_2.system_rhs,
																 redox_pair.carrier_2.old_solution);

		WorkStream::run(electrolyte_dof_handler.begin_active(),
										electrolyte_dof_handler.end(),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		assemble_local_electrolyte_rhs,
																		this, // Assembler object
																		std_cxx11::_1,
																		std_cxx11::_2,
																		std_cxx11::_3),
										std_cxx11::bind(&SOLARCELL::SolarCellProblem<dim>::
																		copy_local_to_global_electrolyte_system_rhs,
																		this, // this object
																		std_cxx11::_1),
										Assembly::AssemblyScratch<dim>(Poisson_fe,
																									 carrier_fe,
																									 QGauss<dim>(degree+2),
																									 QGauss<dim-1>(degree+2)),
										Assembly::DriftDiffusion::CopyData<dim>(carrier_fe)
										);

	}

	template<int dim>
	void 
	SolarCellProblem<dim>::assemble_local_electrolyte_rhs(
								const typename DoFHandler<dim>::active_cell_iterator & cell,
								Assembly::AssemblyScratch<dim>											 & scratch,
								Assembly::DriftDiffusion::CopyData<dim>							 & data)									
	{	
		///////////////////////////////////////////////////////////////////////
		// This is for the case of a coupled Poisson's equation and
		// reactive interface
		//////////////////////////////////////////////////////////////////////

		// this assembles the drift term in the ldg formulation.  it uses the 
		// electric field at the current iteration and the density of the 
		// carrier at the previous time step
		const unsigned int dofs_per_cell			 = 
															scratch.carrier_fe_values.dofs_per_cell;
		const unsigned int n_q_points					 = 
																scratch.carrier_fe_values.n_quadrature_points;
		const unsigned int n_face_q_points		 = 
																scratch.carrier_fe_face_values.n_quadrature_points;

		cell->get_dof_indices(data.local_dof_indices);

		// need to get the cell for poisson triangulation which corresponds 
		// to this cell
		typename DoFHandler<dim>::active_cell_iterator
							Poisson_cell(&electrolyte_triangulation,
													cell->level(),
													cell->index(),
													&Poisson_dof_handler);

		// reinitialize the fe_values 
		scratch.carrier_fe_values.reinit(cell);
		scratch.Poisson_fe_values.reinit(Poisson_cell);
		
		// reset the local_rhs to be zero
		data.local_carrier_1_rhs=0;
		data.local_carrier_2_rhs=0;
	
		const FEValuesExtractors::Vector Current(0);
		const FEValuesExtractors::Scalar Density(dim);

		const FEValuesExtractors::Vector ElectricField(0);

		// get the values of carrier_1 and carrier_2 densities at the pevious time step
		scratch.carrier_fe_values[Density].get_function_values(
																				redox_pair.carrier_1.old_solution,
																				scratch.old_carrier_1_density_values);

		scratch.carrier_fe_values[Density].get_function_values(
																				redox_pair.carrier_2.old_solution,
																				scratch.old_carrier_2_density_values);

		// get the electric field values at the previous time step
		scratch.Poisson_fe_values[ElectricField].get_function_values(
																									Poisson_object.solution,
																									scratch.electric_field_values);

		const double inverse_perm			=		1.0/sim_params.electrolyte_permittivity;

		// loop over all the quadrature points in this cell and compute body integrals
		for(unsigned int q=0; q<n_q_points; q++)
		{
			// loop over all the test function dofs and get the test functions
			for(unsigned int i=0; i<dofs_per_cell; i++)
			{

				const Tensor<1,dim>		psi_i_field		=
																	scratch.carrier_fe_values[Current].value(i,q);
	
				// contribution from RHS function + Drift
				// int_{Omega} v * R dx
				data.local_carrier_1_rhs(i) += ( 
														redox_pair.carrier_1.charge_sign *
														psi_i_field *
														inverse_perm *	
														scratch.electric_field_values[q] *
														scratch.old_carrier_1_density_values[q]
														) *
														scratch.carrier_fe_values.JxW(q);

				data.local_carrier_2_rhs(i) += ( 
														redox_pair.carrier_2.charge_sign *
														psi_i_field *
														inverse_perm *
														scratch.electric_field_values[q] *
														scratch.old_carrier_2_density_values[q]
														) *
														scratch.carrier_fe_values.JxW(q);
	
			} // for i
		}	// for q
		
		// loop over all the faces of this cell and compute the contribution 
		// from the boundary conditions
		for(unsigned int face_no=0; 
				face_no< GeometryInfo<dim>::faces_per_cell;
				face_no++)
		{
			// get the face_no-th face of this cell
			typename DoFHandler<dim>::face_iterator 	face = cell->face(face_no);
			
			// if on boundary apply boundayr conditions	
			if(face->at_boundary() )
			{
				// reinitialize the fe_face_values for this cell ONLY if it is as the
				//boundary otherwise its a waste.  then assemble the appropriate
				//boundary conditions
				scratch.carrier_fe_face_values.reinit(cell, face_no);
			
				if(face->boundary_id() == Dirichlet)
				{
					// Get the doping profile values for the boundary conditions
					reductants_e.value_list(
														scratch.carrier_fe_face_values.get_quadrature_points(),
														scratch.carrier_1_bc_values,
														dim); // calls the density values of the donor profile
																	// not the current ones
					oxidants_e.value_list(
													scratch.carrier_fe_face_values.get_quadrature_points(),
													scratch.carrier_2_bc_values,
													dim); // calls the density values of the donor profile
																// not the current ones

					// loop over all the quadrature points on this face
					for(unsigned int q=0; q<n_face_q_points; q++)
					{
						// loop over all the test function dofs on this face
						for(unsigned int i=0; i<dofs_per_cell; i++)
						{
							// get the test function
							const Tensor<1, dim>  psi_i_field	= 
													scratch.carrier_fe_face_values[Current].value(i,q);
 
							// int_{\Gamma_{D}} -p^{-} n^{-} u_{D} ds
							data.local_carrier_1_rhs(i) += 
																-1.0 * psi_i_field *
													 			 scratch.carrier_fe_face_values.normal_vector(q) *
																 scratch.carrier_1_bc_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q);				
					
							data.local_carrier_2_rhs(i) +=  
																-1.0 * psi_i_field *
													 			 scratch.carrier_fe_face_values.normal_vector(q) *
	  														  scratch.carrier_2_bc_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q);				


						} // for i
					}	// for q
				} // end Dirichlet
				else if(face->boundary_id() == Interface)
				{
					// Get the doping profile values for the boundary conditions
					electrons_e.value_list(
														scratch.carrier_fe_face_values.get_quadrature_points(),
														scratch.carrier_1_bc_values,
														dim); // calls the density values of the donor profile
																	// not the current ones
					holes_e.value_list(
													scratch.carrier_fe_face_values.get_quadrature_points(),
													scratch.carrier_2_bc_values,
													dim); // calls the density values of the donor profile
																// not the current ones
					
	
					// get the electrona and hole densities at the pevious time step
					scratch.carrier_fe_face_values[Density].get_function_values(
																				electron_hole_pair.carrier_1.old_solution,
																				scratch.electron_interface_values);

					scratch.carrier_fe_face_values[Density].get_function_values(
																				electron_hole_pair.carrier_2.old_solution,
																				scratch.hole_interface_values);

					// get the electrona and hole densities at the pevious time step
					scratch.carrier_fe_face_values[Density].get_function_values(
																				redox_pair.carrier_1.old_solution,
																				scratch.reductant_interface_values);

					scratch.carrier_fe_face_values[Density].get_function_values(
																				redox_pair.carrier_2.old_solution,
																				scratch.oxidant_interface_values);

					// loop over all the quadrature points on this face
					for(unsigned int q=0; q<n_face_q_points; q++)
					{
						// loop over all the test function dofs on this face
						for(unsigned int i=0; i<dofs_per_cell; i++)
						{
							// get the test function
							const double psi_i_density =
											scratch.carrier_fe_face_values[Density].value(i,q);

							const double current = -1.0 * psi_i_density *
																 sim_params.scaled_k_et *
																 (scratch.electron_interface_values[q]
																	-
																	scratch.carrier_1_bc_values[q]) * 
																 scratch.oxidant_interface_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q) 
																 +1.0 * psi_i_density *
																 sim_params.scaled_k_ht *
																 (scratch.hole_interface_values[q]
																	-
																	scratch.carrier_2_bc_values[q]) *
																 scratch.reductant_interface_values[q] *
															 	 scratch.carrier_fe_face_values.JxW(q);				
		
	
							// int_{\Gamma_{D}} -v^{-} ket (rho_n - rho_n^e) rho_o ds 
							// 	+
							// int_{\Gamma_{D}}  v^{-} kht (rho_p - rho_p^e) rho_r ds
							data.local_carrier_1_rhs(i) += current;
							data.local_carrier_2_rhs(i) += -1.0 *current;
		
						} // for i
					} // for j
				}
				else if(face->boundary_id() == Neumann)
				{
					// NOTHIN TO DO IF INSULATING
				}
				else
					Assert(false, ExcNotImplemented() );
			} // end at boundary
		} // end for face_no
	
	}	

	template <int dim>
	void 
	SolarCellProblem<dim>::
	set_solvers()
	{
		Poisson_object.set_solver();
		electron_hole_pair.carrier_1.set_solver();
		electron_hole_pair.carrier_2.set_solver();
		redox_pair.carrier_1.set_solver();
		redox_pair.carrier_2.set_solver();
	}

	
	template <int dim>
	void 
	SolarCellProblem<dim>::
	solve_Poisson()
	{
		Poisson_object.solve();	
	}		
	
	template<int dim>
	void
	SolarCellProblem<dim>::
	solve_semiconductor_system()
	{
		// solve carrier_1
		electron_hole_pair.carrier_1.solve();
	
		// solve carrier_2
		electron_hole_pair.carrier_2.solve();
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	solve_electrolyte_system()
	{
		// solve carrier_1
		redox_pair.carrier_1.solve();
	
		// solve carrier_2
		redox_pair.carrier_2.solve();
	}

	template<int dim>
	void 
	SolarCellProblem<dim>::
	output_results()
	{
		PostProcessor<dim> postprocessor(sim_params);
		DataOut<dim> data_out;
		data_out.attach_dof_handler(Poisson_dof_handler);

		data_out.add_data_vector(Poisson_object.solution,
														postprocessor);

		data_out.build_patches();
		std::ofstream output("solution.vtu");
		data_out.write_vtu(output);
	}

	template<int dim>
	void
	SolarCellProblem<dim>::
	run()
	{
		TimerOutput	timer(std::cout,
										 TimerOutput::summary,
										 TimerOutput::wall_times);

		timer.enter_subsection("Make Grids");
	
		// make the triangulations
		Grid_Maker::Grid<dim> grid_maker;

		// make the semiconductor grid
		grid_maker.make_pn_junction_grid(semiconductor_triangulation, 
																			sim_params);
		grid_maker.make_Dirichlet_boundaries(semiconductor_triangulation,
																			sim_params);
//		grid_maker.make_Neumann_boundaries(semiconductor_triangulation);

		// make the electrolyte grid
		grid_maker.make_electrolyte_grid(electrolyte_triangulation, 
																 		sim_params);
		grid_maker.make_Dirichlet_boundaries(electrolyte_triangulation,
																			sim_params);
//		grid_maker.make_Neumann_boundaries(electrolyte_triangulation);
	

		// make the merged semiconductor-electrolyte grid

		grid_maker.make_merged_grid(semiconductor_triangulation, 
																electrolyte_triangulation,
																Poisson_triangulation);
		grid_maker.make_Dirichlet_boundaries(Poisson_triangulation,
																			sim_params);
//		grid_maker.make_Neumann_boundaries(Poisson_triangulation);

		timer.leave_subsection("Make Grids");

		timer.enter_subsection("Allocate Memory");
		// allocate the memory
		distribute_dofs();
		allocate_memory();
		timer.leave_subsection("Allocate Memory");

		// dont remove
		electron_hole_pair.penalty = 
					1.0/GridTools::maximal_cell_diameter(semiconductor_triangulation);

		redox_pair.penalty = 
					1.0/GridTools::maximal_cell_diameter(electrolyte_triangulation);
	
		// make time stepping stuff... needs to be done before assembling
		// LDG matrices
		delta_t = sim_params.delta_t;
		unsigned int time_step_number	= 0;
		double time										= 0.0;
		unsigned int number_outputs = sim_params.time_stamps;
		std::vector<double>								timeStamps(number_outputs);
		
		for(unsigned int i=0; i<number_outputs; i++)
			timeStamps[i] = (i+1) * sim_params.t_end / number_outputs;

//		double delta_t = std::min(electron_hole_pair.delta_t,
//															redox_pair.delta_t);

/*		
		timer.enter_subsection("Assemble Poisson Matrix");
//		assemble the global matrices
		assemble_Poisson_matrix();

		timer.leave_subsection("Assemble Poisson Matrix");


		timer.enter_subsection("Assemble LDG Matrices");
		assemble_mass_matrix(); 

		// the 1.0 means we are calculating transients -> 1.0/delta_t
		assemble_LDG_cell_and_bc_terms(1.0);

		LDG_Assembler.assemble_flux_terms(semiconductor_dof_handler,
												electron_hole_pair,
												Poisson_fe,
												carrier_fe);	

		LDG_Assembler.assemble_flux_terms(electrolyte_dof_handler,
												redox_pair,
												Poisson_fe,
												carrier_fe);	


		timer.leave_subsection("Assemble LDG Matrices");

		timer.enter_subsection("Factor Matrices");
		// factor the matrices
		set_solvers();
		timer.leave_subsection("Factor Matrices");
*/	
				// Get the initial conditions
		VectorTools::project(semiconductor_dof_handler,
												 electron_hole_pair.constraints,
												 QGauss<dim>(degree+1),
												 electrons_e,
												 electron_hole_pair.carrier_1.old_solution);

		electron_hole_pair.carrier_1.initialize();

		VectorTools::project(semiconductor_dof_handler,
												 electron_hole_pair.constraints,
												 QGauss<dim>(degree+1),
												 holes_e,
												 electron_hole_pair.carrier_2.old_solution);

		electron_hole_pair.carrier_2.initialize();

		VectorTools::project(electrolyte_dof_handler,
												 redox_pair.constraints,
												 QGauss<dim>(degree+1),
												 reductants_e,
												 redox_pair.carrier_1.old_solution);

		redox_pair.carrier_1.initialize();

		VectorTools::project(electrolyte_dof_handler,
												 redox_pair.constraints,
												 QGauss<dim>(degree+1),
												 oxidants_e,
												 redox_pair.carrier_2.old_solution);
	
		redox_pair.carrier_2.initialize();
/*
		// get the intitial potential and electric field
		assemble_Poisson_rhs();
		solve_Poisson();
*/
		// print the results
		LDG_Assembler.output_results(semiconductor_dof_handler,
																electron_hole_pair,
																time_step_number);	

		LDG_Assembler.output_results(electrolyte_dof_handler,
																redox_pair,
																time_step_number);	

/*		Mixed_Assembler.output_results(Poisson_dof_handler,
																	Poisson_object.solution,
																	time_step_number);
	
		// time stepping!
		for(time_step_number = 1;
				time_step_number<=number_outputs;
				time_step_number++)
		{
			std::cout << "time = " << time << std::endl;

			while(time < timeStamps[time_step_number -1])
			{
				timer.enter_subsection("Assemble semiconductor rhs");
				assemble_semiconductor_rhs();
				timer.leave_subsection("Assemble semiconductor rhs");

				timer.enter_subsection("Solve semiconductor system");
				solve_semiconductor_system();
				timer.leave_subsection("Solve semiconductor system");

				timer.enter_subsection("Assemble electrolyte rhs");
				assemble_electrolyte_rhs();
				timer.leave_subsection("Assemble electrolyte rhs");

				timer.enter_subsection("Solve electrolyte system");
				solve_electrolyte_system();
				timer.leave_subsection("Solve electrolyte system");

				timer.enter_subsection("Assemble Poisson rhs");
				assemble_Poisson_rhs();
				timer.leave_subsection("Assemble Poisson rhs");

				timer.enter_subsection("Solve Poisson system");
				solve_Poisson();			
				timer.leave_subsection("Solve Poisson system");

				time += delta_t;
			} // while

			// print the results
			timer.enter_subsection("Printing");
			LDG_Assembler.output_results(semiconductor_dof_handler,
																electron_hole_pair,
																time_step_number);	

			LDG_Assembler.output_results(electrolyte_dof_handler,
																	redox_pair,
																	time_step_number);	

			Mixed_Assembler.output_results(Poisson_dof_handler,
																	Poisson_object.solution,
																	time_step_number);
		
			timer.leave_subsection("Printing");

		} // for 
*/
	}


	template<int dim>
	void
	SolarCellProblem<dim>::
	test_steady_state(const unsigned int & n_refine,
										ConvergenceTable & Mixed_table,
										ConvergenceTable & LDG_table)
	{
		double 	primary_error, flux_error;
	
		Grid_Maker::Grid<dim> grid_maker;
		grid_maker.make_cube(Poisson_triangulation, n_refine);

		grid_maker.make_cube(semiconductor_triangulation, n_refine);

		grid_maker.make_cube(electrolyte_triangulation, n_refine);

		distribute_dofs();
		allocate_memory();

		sim_params.semiconductor_permittivity = 1.0;
		sim_params.electrolyte_permittivity 	= 1.0;
		sim_params.scaled_debeye_length 			= 1.0;

		assemble_Poisson_matrix();

		electron_hole_pair.delta_t = 1.0;
		electron_hole_pair.carrier_1.scaled_mobility = 1;
		electron_hole_pair.carrier_2.scaled_mobility = 1;
		electron_hole_pair.penalty = 
					1.0/GridTools::maximal_cell_diameter(semiconductor_triangulation);

		redox_pair.delta_t = 1.0;
		redox_pair.carrier_1.scaled_mobility = 1;
		redox_pair.carrier_2.scaled_mobility = 1;
	
		redox_pair.penalty = 
					1.0/GridTools::maximal_cell_diameter(electrolyte_triangulation);
	
		assemble_mass_matrix(); 
		assemble_LDG_cell_and_bc_terms(0.0);

		LDG_Assembler.assemble_flux_terms(semiconductor_dof_handler,
												electron_hole_pair,
												Poisson_fe,
												carrier_fe);	

		LDG_Assembler.assemble_flux_terms(electrolyte_dof_handler,
												redox_pair,
												Poisson_fe,
												carrier_fe);	


//		std::ofstream output("A.mtx");
//		electron_hole_pair.carrier_1.system_matrix.print_formatted(output);
//		output.close();

		set_solvers();

		unsigned int n_active_cells = Poisson_triangulation.n_active_cells();
		unsigned int n_dofs					= Poisson_dof_handler.n_dofs();
		
		Mixed_table.add_value("cells", n_active_cells);
		Mixed_table.add_value("dofs", n_dofs);

		n_active_cells = semiconductor_triangulation.n_active_cells();
		n_dofs				 = semiconductor_dof_handler.n_dofs();

		LDG_table.add_value("cells", n_active_cells);
		LDG_table.add_value("dofs", n_dofs);
	
		assemble_test_Poisson_rhs();
		assemble_test_LDG_rhs();

		solve_Poisson();
		solve_semiconductor_system();

		Mixed_Assembler.compute_errors(Poisson_triangulation, 
																	 Poisson_dof_handler,
																	 Poisson_object.solution,
																	 primary_error,
																	 flux_error);
	
		Mixed_table.add_value("Phi", primary_error);
		Mixed_table.add_value("D", flux_error);
		Mixed_Assembler.output_results(Poisson_dof_handler,
																	 Poisson_object.solution,
																	 n_refine);

	
		LDG_Assembler.compute_errors(semiconductor_triangulation,
																 semiconductor_dof_handler,
																 electron_hole_pair.carrier_1.solution,
																 primary_error,
																 flux_error);

		LDG_table.add_value("u", primary_error);
		LDG_table.add_value("J", flux_error);
		
		LDG_Assembler.output_results(semiconductor_dof_handler,
																 electron_hole_pair,
																 n_refine);	

	}

			

} // namespace SOLARCELL
