#include "../include/Grid.hpp"


/////////////////////////////////////////////////////////////////////////////////
// GRID AND BOUNDARIES
/////////////////////////////////////////////////////////////////////////////////
namespace Grid_Maker
{
	using namespace dealii;

	template<int dim>
	Grid<dim>::
	Grid()
	{
		// nothing
	}

	template<int dim>
	void
	Grid<dim>::
	make_semiconductor_grid(Triangulation<dim> 	& triangulation,
													ParameterSpace::Parameters 		 & params)
	{
//		const unsigned int dim = 2;
		static const Point<2> vertices_1[]
			= { Point<2>(0,0),
					Point<2>(params.scaled_radius_two, 0),
					Point<2>(0,params.scaled_domain_height),
					Point<2>(params.scaled_radius_one,  params.scaled_domain_height)
				};

		const unsigned int n_vertices = sizeof(vertices_1)/sizeof(vertices_1[0]);

		const std::vector<Point<dim>> vertices(&vertices_1[0],
																					 &vertices_1[n_vertices]);

		static const int cell_vertices[][GeometryInfo<dim>::vertices_per_cell]
			= {  {0,1,2,3}
		 	  };

		const unsigned int n_cells = sizeof(cell_vertices)/sizeof(cell_vertices[0]);
	
		std::vector<CellData<dim> > cells(n_cells, CellData<dim>() );
		for(unsigned int i=0; i<n_cells; i++)
		{
			for(unsigned int j=0;
					j<GeometryInfo<dim>::vertices_per_cell;
					j++)
			{
				cells[i].vertices[j] = cell_vertices[i][j];
			}
			cells[i].material_id = 0;
		}

		// create a temporary triangulation, refined it, print it 
		// and read it back in.		
		Triangulation<dim>	temp_triangulation;
		temp_triangulation.create_triangulation(vertices,
																						cells,
																						SubCellData());

		temp_triangulation.refine_global(params.n_global_refine);
		

		std::string temp_triangulation_name = "temp.msh";
		output_mesh(temp_triangulation,
							temp_triangulation_name);

		// we do this so that all the cells are on level 0
		// and can be merged with another triangulation later
		GridIn<dim> grid_in;
		grid_in.attach_triangulation(triangulation);
		std::ifstream	input_file(temp_triangulation_name);
		grid_in.read_msh(input_file);

		typename Triangulation<dim>::active_cell_iterator 
																	cell = triangulation.begin_active(),
																	endc = triangulation.end();
		// set the material id
		for(; cell != endc; cell++)
		{
			cell->set_material_id(semiconductor_id);
		}

	}


	template<int dim>
	void
	Grid<dim>::
	make_pn_junction_grid(Triangulation<dim> 	& triangulation,
												ParameterSpace::Parameters 		 & params)
	{
		double junction_location = 0.2;

		static const Point<2> vertices_2_temp[]
			= { Point<2>(0,0),
					Point<2>(junction_location, 0),
					Point<2>(params.scaled_radius_two, 0),
					Point<2>(0, params.scaled_domain_height),
					Point<2>(junction_location, params.scaled_domain_height),
					Point<2>(params.scaled_radius_one,  params.scaled_domain_height)
				};

		const unsigned int n_vertices_2_temp
							 = sizeof(vertices_2_temp)/sizeof(vertices_2_temp[0]);

		const std::vector<Point<dim>> vertices_2(&vertices_2_temp[0],
																					 	&vertices_2_temp[n_vertices_2_temp]);

		static const int cell_vertices_2[][GeometryInfo<dim>::vertices_per_cell]
						= {  {0,1,3,4}, // cell 1
									{1,2,4,5} // cell 2
		 				  };

		const unsigned int n_cells_2 = sizeof(cell_vertices_2)/sizeof(cell_vertices_2[0]);
	
		std::vector<CellData<dim> > cells_2(n_cells_2, CellData<dim>() );
		for(unsigned int i=0; i<n_cells_2; i++)
		{
			for(unsigned int j=0;
					j<GeometryInfo<dim>::vertices_per_cell;
					j++)
			{
				cells_2[i].vertices[j] = cell_vertices_2[i][j];
			}
			cells_2[i].material_id = 0;
		}

		// create a temporary triangulation, refined it, print it 
		// and read it back in.		
		Triangulation<dim>	temp_triangulation_2;
		temp_triangulation_2.create_triangulation(vertices_2,
																							cells_2,
																							SubCellData());

		temp_triangulation_2.refine_global(params.n_global_refine);
		std::string temp_triangulation_name = "temp.msh";

		output_mesh(temp_triangulation_2,
							 temp_triangulation_name);


		// we do this so that all the cells are on level 0
		// and can be merged with another triangulation later
		GridIn<dim> grid_in;

		grid_in.attach_triangulation(triangulation);
		std::ifstream	input_file(temp_triangulation_name);
		grid_in.read_msh(input_file);

		typename Triangulation<dim>::active_cell_iterator 
																	cell = triangulation.begin_active(),
																	endc = triangulation.end();
		// set the material id
		for(; cell != endc; cell++)
		{
			cell->set_material_id(semiconductor_id);
		}
		
	}

	template<int dim>
	void
	Grid<dim>::
	make_electrolyte_grid(Triangulation<dim> 	& triangulation,
													ParameterSpace::Parameters 		 & params)
	{
//		const unsigned int dim = 2;
		static const Point<2> vertices_1[]
		= {		Point<2>(params.scaled_radius_two, 0),
					Point<2>(params.scaled_domain_length, 0),
					Point<2>(params.scaled_radius_one, params.scaled_domain_height),
					Point<2>(params.scaled_domain_length, params.scaled_domain_height)
				};

		const unsigned int n_vertices = sizeof(vertices_1)/sizeof(vertices_1[0]);

		const std::vector<Point<dim>> vertices(&vertices_1[0],
																					 &vertices_1[n_vertices]);

		static const int cell_vertices[][GeometryInfo<dim>::vertices_per_cell]
			= {  {0,1,2,3}
		 	  };

		const unsigned int n_cells = sizeof(cell_vertices)/sizeof(cell_vertices[0]);
	
		std::vector<CellData<dim> > cells(n_cells, CellData<dim>() );
		for(unsigned int i=0; i<n_cells; i++)
		{
			for(unsigned int j=0;
					j<GeometryInfo<dim>::vertices_per_cell;
					j++)
			{
				cells[i].vertices[j] = cell_vertices[i][j];
			}
			cells[i].material_id = 0;
		}

		// create a temporary triangulation, refined it, print it 
		// and read it back in.		
		Triangulation<dim>	temp_triangulation;
		temp_triangulation.create_triangulation(vertices,
																						cells,
																						SubCellData());

		temp_triangulation.refine_global(params.n_global_refine);


		std::string temp_triangulation_name = "temp.msh";
		output_mesh(temp_triangulation,
							temp_triangulation_name);

		// we do this so that all the cells are on level 0
		// and can be merged with another triangulation later
		GridIn<dim> grid_in;
		grid_in.attach_triangulation(triangulation);
		std::ifstream	input_file(temp_triangulation_name);
		grid_in.read_msh(input_file);

		typename Triangulation<dim>::active_cell_iterator 
																	cell = triangulation.begin_active(),
																	endc = triangulation.end();

		// set the material id
		for(; cell != endc; cell++)
		{
			cell->set_material_id(electrolyte_id);
		}

	}


	template <int dim>
	void 
	Grid<dim>::
	make_merged_grid(const Triangulation<dim>				 & semiconductor_triang,
									const Triangulation<dim>				 & electrolyte_triang,
									Triangulation<dim>							 & merged_triangulation)
	{
		// merges the two triangulations in to one
		GridGenerator::merge_triangulations(semiconductor_triang,
																				electrolyte_triang,
																				merged_triangulation);
	}

	template <int dim>
	void 
	Grid<dim>::
	make_cube(Triangulation<dim> 						& triangulation,
						const int											& n_global_refine)		
	{
		// make the triangulation and refine globally n_refine times
		GridGenerator::hyper_cube(triangulation,0,1);
		triangulation.refine_global(n_global_refine);
	
		// set the boundaries to be Dirichlet
		typename Triangulation<dim>::cell_iterator
																		cell = triangulation.begin(),
																		endc = triangulation.end();
		// loop over all the cells
		for(; cell != endc; cell++)
		{
			// loop over all the faces of the cell and find which are on the boundary
			for(unsigned int face_no=0;
					face_no < GeometryInfo<dim>::faces_per_cell;
					face_no++)
			{
				if(cell->face(face_no)->at_boundary() )
				{
					//NOTE: Default is 0, which implies Interface
					cell->face(face_no)->set_boundary_id(Dirichlet);
				} // end if on boundary
			} // for face_no
		} // for cell

	}

	template <int dim>
	void 
	Grid<dim>::
	make_local_refined_cube(Triangulation<dim> & triangulation,
													ParameterSpace::Parameters 				& params)
	{
	// make the triangulation and refine globally n_refine times
		GridGenerator::hyper_cube(triangulation,0,1);
		triangulation.refine_global(params.n_global_refine);
		
		// loop over the number of refinements and at each iteration
		// mark the cells which need to be refined and then refine them

		// loop over the number of local refinements 
		for(unsigned int i =0; i < params.n_local_refine; i++)
		{
			typename Triangulation<dim>::active_cell_iterator 
																	cell = triangulation.begin_active(),
																	endc = triangulation.end();

			// loop over all the cells and mark which ones need to be refined
			for(; cell != endc; cell++)
			{
//			if(cell->at_boundary())
				if( (cell->center()[1] < 0.6 ) && 
						(cell->center()[1] > 0.4 ) )
				{
//					if( (cell->center()[1] < 0.3 ) )
//									if((cell->center()[0] > 0.9)  ||
//						 	 (cell->center()[0] < 0.1) )
					{
						cell->set_refine_flag();
					} // if on x coordinate
				} // if on y coordinate
			} // for cell

			// refine them in an appropriate way such that neighbor cells
			// do not have have more than one refinement level difference
			triangulation.execute_coarsening_and_refinement();
		}

		params.h_min = GridTools::minimal_cell_diameter(triangulation);
		params.h_max = GridTools::maximal_cell_diameter(triangulation);
		
		params.penalty = 1.0/params.h_max;	

	}

	template<int dim> 
	void 
	Grid<dim>::
	make_Dirichlet_boundaries(Triangulation<dim> & triangulation,
														ParameterSpace::Parameters 		 & params)
	{
		typename Triangulation<dim>::cell_iterator
																		cell = triangulation.begin(),
																		endc = triangulation.end();
		// loop over all the cells
		for(; cell != endc; cell++)
		{
			// loop over all the faces of the cell and find which are on the boundary
			for(unsigned int face_no=0;
					face_no < GeometryInfo<dim>::faces_per_cell;
					face_no++)
			{
				if(cell->face(face_no)->at_boundary() )
				{
					//NOTE: Default is 0, which implies Interface

					//sets only the non-interface boundary to be Dirichlet	
					if((cell->face(face_no)->center()[0] == 0.0) ||
						(cell->face(face_no)->center()[0] == params.scaled_domain_length) ||
						(cell->face(face_no)->center()[1] == 0.0) ||
						(cell->face(face_no)->center()[1] == params.scaled_domain_height) )
						{
							cell->face(face_no)->set_boundary_id(Dirichlet);
						}
				} // end if on boundary
			} // for face_no
		} // for cell

	} 

	template<int dim> 
	void 
	Grid<dim>::
	make_Neumann_boundaries(Triangulation<dim> & triangulation)
	{
		typename Triangulation<dim>::cell_iterator
																		cell = triangulation.begin(),
																		endc = triangulation.end();
		// loop over all the cells
		for(; cell != endc; cell++)
		{
			// loop over all the faces of the cell and find which are on the bondary
			for(unsigned int face_no=0;
					face_no < GeometryInfo<dim>::faces_per_cell;
					face_no++)
			{
				if(cell->face(face_no)->at_boundary() )
				{
					// set the portions of the boundary y = 0 and y = +1 
					// to be Neumann boundary conditions
					if((cell->face(face_no)->center()[1] == 0) ||
						 (cell->face(face_no)->center()[1] == 1.0) )
					{
						// set it to be Neumann boundary condition by setting the boundary 
						// indicator to be 1.  NOTE: Default is 0, which implies Dirichlet
						cell->face(face_no)->set_boundary_id(Neumann);
					} // end if Neumann segment 
				} // end if on boundary
			} // for face_no
		} // for cell

	}  

	template<int dim>
	void
	Grid<dim>::
	output_mesh(Triangulation<dim> & triangulation,
							const std::string & 				grid_name)
	{
		std::ofstream out(grid_name.c_str());
//		std::ofstream out("grid.eps");
		GridOut grid_out;
		grid_out.write_msh(triangulation,out);
		out.close();
	
	}

	template<int dim> 
	void 
	Grid<dim>::
	print_grid(Triangulation<dim> & triangulation,
						const std::string & 				grid_name)
	{	
		std::ofstream out(grid_name.c_str());
//		std::ofstream out("grid.eps");
		GridOut grid_out;
		grid_out.write_eps(triangulation,out);
		out.close();
	}



} 
