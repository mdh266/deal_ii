#include "../include/ParameterReader.hpp"

namespace ParameterSpace
{
	using namespace dealii;
	
	ParameterReader::
	ParameterReader(ParameterHandler & param_handler) 
	:
	prm(param_handler)
	{}

	void
	ParameterReader:: 
	read_parameters(const std::string parameter_file)
	{
		declare_parameters();
		prm.read_input(parameter_file);
	}

	void
	ParameterReader:: 
	read_test_parameters(const std::string parameter_file)
	{
		declare_test_parameters();
		prm.read_input(parameter_file);
	}

	void 
	ParameterReader::
	declare_parameters()
	{
		prm.enter_subsection("computational");
		prm.declare_entry("global refinements", "4",
											Patterns::Integer(1,10),
											"number of global refinements");

		prm.declare_entry("local refinements", "0",
									Patterns::Integer(0,10),
									"number of local refinements in predefined critical areas");

		prm.declare_entry("time step size", "0.01",
											Patterns::Double(0,1),
											"scaled time step size");

		prm.declare_entry("time stamps", "100",
											Patterns::Integer(0,1000),
											"number of output files");

		prm.leave_subsection();				

		prm.enter_subsection("physical");

		prm.declare_entry("illumination status", "0",
								Patterns::Bool(),
								"true means that the cell is illuminated, false means its not.");

		prm.declare_entry("characteristic length", "1.0e-4",
											Patterns::Double(0),
											"the characteristic length scale [cm]");

		prm.declare_entry("characteristic density", "1.0e16",
											Patterns::Double(0),
											"the characteristic density scale [cm^{-3}]");

		prm.declare_entry("characteristic time", "1.0e-12",
											Patterns::Double(0),
											"the characteristic time scale [s]");

		prm.declare_entry("end time", "1",
											Patterns::Double(0),
											"physical end time (in terms of characteristic time)");

		prm.declare_entry("intrinsic density", "2.564e9",
											Patterns::Double(0),
											"the intrinsic density scale [cm^{-3}]");

		prm.declare_entry("semiconductor permittivity", "11.9",
											Patterns::Double(0),
											"semiconductor permittivity const []");

		prm.declare_entry("electrolyte permittivity", "1000",
											Patterns::Double(0),
											"electrolyte permittivity const []");

		prm.declare_entry("photon flux", "1.2e17",
											Patterns::Double(0),
											"intensity of light  [cm^{-2}s^{-1} ]");
	
		prm.declare_entry("absorption coefficient", "1.74974e5",
						Patterns::Double(0),
						"absoprtion coefficient averaged over all energies  [cm^{-1} ]");

		prm.leave_subsection();

		prm.enter_subsection("mesh");
		prm.declare_entry("mesh length", "2.0",
											Patterns::Double(0),
											"scaled domain length in x coordinate");

		prm.declare_entry("mesh height", "1",
											Patterns::Double(0),
											"scaled domain height in y coordinate");
	
		prm.declare_entry("radius one", "1.0",
											Patterns::Double(0),
											"top radius of wire");

		prm.declare_entry("radius two", "1.0",
											Patterns::Double(0),
											"bottom radius of wire");

		prm.leave_subsection();	

		prm.enter_subsection("electrons");

		prm.declare_entry("mobility", "1350.0", 
											Patterns::Double(0),
											"electron mobility [v/cm^{2}]");	

		prm.declare_entry("transfer rate", "1e-19",
											 Patterns::Double(0),
											"transfer rate of electrons [cm^{4} s^{-1}]");


		prm.declare_entry("recombination time", "5e-5",
											 Patterns::Double(0),
											"Recombination rate/time of electrons [s]");

		prm.leave_subsection();	
	
		prm.enter_subsection("holes");

		prm.declare_entry("mobility", "480.0", 
											Patterns::Double(0),
											"hole mobility [v/cm^{2}]");	

		prm.declare_entry("transfer rate", "1e-16",
											 Patterns::Double(0),
											"transfer rate of holes [cm^{4} s^{-1}]");

		prm.declare_entry("recombination time", "5e-5",
											 Patterns::Double(0),
											"Recombination rate/time of holes [s]");

		prm.leave_subsection();	

		prm.enter_subsection("reductants");

		prm.declare_entry("mobility", "1.0", 
											Patterns::Double(0),
											"reductant mobility [v/cm^{2}]");	
	
		prm.leave_subsection();
	
		prm.enter_subsection("oxidants");

		prm.declare_entry("mobility", "1.0", 
											Patterns::Double(0),
											"oxidant mobility [v/cm^{2}]");	
	
		prm.leave_subsection();
	


	}
	void 
	ParameterReader::
	declare_test_parameters()
	{
		prm.enter_subsection("computational");
		prm.declare_entry("global refinements", "3",
											Patterns::Integer(1,10),
											"number of global refinements");

		prm.declare_entry("local refinements", "0",
									Patterns::Integer(0,10),
									"number of local refinements in predefined critical areas");

		prm.declare_entry("time step size", "0.01",
											Patterns::Double(0,1),
											"scaled time step size");

		prm.declare_entry("time stamps", "100",
											Patterns::Integer(0,1000),
											"number of output files");

		prm.leave_subsection();				

		prm.enter_subsection("physical");

		prm.declare_entry("illumination status", "Off",
								Patterns::Anything(),
								"On means that the cell is illuminated, off means its not.");

		prm.declare_entry("characteristic length", "1.0",
											Patterns::Double(0),
											"the characteristic length scale [cm]");

		prm.declare_entry("characteristic density", "1.0",
											Patterns::Double(0),
											"the characteristic density scale [cm^{-3}]");

		prm.declare_entry("characteristic time", "1.0",
											Patterns::Double(0),
											"the characteristic time scale [s]");

		prm.declare_entry("end time", "1",
											Patterns::Double(0),
											"physical end time (in terms of characteristic time)");

		prm.declare_entry("intrinsic density", "1.0",
											Patterns::Double(0),
											"the intrinsic density scale [cm^{-3}]");

		prm.declare_entry("semiconductor permittivity", "1.0",
											Patterns::Double(0),
											"semiconductor permittivity const []");

		prm.declare_entry("electrolyte permittivity", "1.0",
											Patterns::Double(0),
											"electrolyte permittivity const []");

		prm.declare_entry("photon flux", "0.0",
											Patterns::Double(0),
											"intensity of light  [cm^{-2}s^{-1} ]");
	
		prm.declare_entry("absorption coefficient", "0.0",
						Patterns::Double(0),
						"absoprtion coefficient averaged over all energies  [cm^{-1} ]");

		prm.leave_subsection();

		prm.enter_subsection("mesh");
		prm.declare_entry("mesh length", "2",
											Patterns::Double(0),
											"scaled domain length in x coordinate");

		prm.declare_entry("mesh height", "1",
											Patterns::Double(0),
											"scaled domain height in y coordinate");
	
		prm.declare_entry("radius one", "1.0",
											Patterns::Double(0),
											"top radius of wire");

		prm.declare_entry("radius two", "1.0",
											Patterns::Double(0),
											"bottom radius of wire");

		prm.leave_subsection();	

		prm.enter_subsection("electrons");

		prm.declare_entry("mobility", "1.0", 
											Patterns::Double(0),
											"electron mobility [v/cm^{2}]");	

		prm.declare_entry("transfer rate", "0",
											 Patterns::Double(0),
											"transfer rate of electrons [cm^{4} s^{-1}]");


		prm.declare_entry("recombination time", "0",
											 Patterns::Double(0),
											"Recombination rate/time of electrons [s]");

		prm.leave_subsection();	
	
		prm.enter_subsection("holes");

		prm.declare_entry("mobility", "1.0", 
											Patterns::Double(0),
											"hole mobility [v/cm^{2}]");	

		prm.declare_entry("transfer rate", "0",
											 Patterns::Double(0),
											"transfer rate of holes [cm^{4} s^{-1}]");

		prm.declare_entry("recombination time", "0",
											 Patterns::Double(0),
											"Recombination rate/time of holes [s]");

		prm.leave_subsection();	

		prm.enter_subsection("reductants");

		prm.declare_entry("mobility", "1.0", 
											Patterns::Double(0),
											"reductant mobility [v/cm^{2}]");	
	
		prm.leave_subsection();
	
		prm.enter_subsection("oxidants");

		prm.declare_entry("mobility", "1.0", 
											Patterns::Double(0),
											"oxidant mobility [v/cm^{2}]");	
	
		prm.leave_subsection();
	


	}
										 
}


