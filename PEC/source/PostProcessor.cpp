#include "../include/PostProcessor.hpp"

using namespace dealii;

template<int dim>
PostProcessor<dim>::
PostProcessor(const ParameterSpace::Parameters & sim_params)
{
	scale_density 	 = sim_params.characteristic_denisty;

	scale_potential  = 0.02585;

	scale_current  	 = sim_params.characteristic_denisty *
											sim_params.characteristic_length / 
											sim_params.characteristic_time;

	scale_elec_field = 0.2585/sim_params.characteristic_length;
	
	std::cout << "scale_density = " << scale_density << std::endl;
	std::cout << "scale_potential = " << scale_potential << std::endl;
	std::cout << "scale_current = " << scale_current << std::endl;
	std::cout << "scale_elec_field = " << scale_elec_field << std::endl;
}

template<int dim>
std::vector<std::string> 
PostProcessor<dim>::
get_names() const
{
	// poisson electric field and potential
	std::vector<std::string> solution_names(dim,"Field");
	solution_names.push_back("Potential");

	return solution_names;
}

template<int dim>
std::vector<DataComponentInterpretation::DataComponentInterpretation>
PostProcessor<dim>::
get_data_component_interpretation() const
{

	// electric field
	std::vector<DataComponentInterpretation::DataComponentInterpretation>
	interpretation(dim,
								DataComponentInterpretation::component_is_part_of_vector);

	// potential
	interpretation.push_back(DataComponentInterpretation::component_is_scalar);

	return interpretation;
}

template<int dim>
UpdateFlags
PostProcessor<dim>::
get_needed_update_flags() const
{
	return update_values | update_gradients;
}


template<int dim>
void 
PostProcessor<dim>::
compute_derived_quantities_vector(
	const std::vector<Vector<double>>								&uh,
	const std::vector<std::vector<Tensor<1,dim>>>		&duh,
	const std::vector<std::vector<Tensor<2,dim>>>		&/* dduh*/,
	const std::vector<Point<dim>>										&/*normals*/,
	const std::vector<Point<dim>>										&/*evaluated_points*/,
	std::vector<Vector<double>>											&computed_quantities) const
{

	const unsigned int n_quadrature_points = uh.size();
	Assert(duh.size() == n_quadrature_points, ExcInternalError());
	Assert(computed_quantities.size() == n_quadrature_points, ExcInternalError());
	Assert(uh[0].size() == dim+1, ExcInternalError());
	
	for(unsigned int q=0; q<n_quadrature_points; q++)
	{
		// copy over electric field..
		// TODO: rescale with permitivities?
		for(unsigned int d=0; d<dim; d++)
			computed_quantities[q](d) = (scale_elec_field * uh[q](d));
	
		// copy over potential
		const double potential = scale_potential * uh[q](dim);
		computed_quantities[q](dim) = potential;
 
	} // end for

 }


