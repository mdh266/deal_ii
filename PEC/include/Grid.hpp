#ifndef _GRID_H__
#define _GRID_H__
#include <deal.II/grid/tria.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/grid_refinement.h>
//#include <deal.II/base/parameter_handler.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/grid/grid_in.h>
#include <iostream>
#include <fstream>
#include <string>
#include "Parameters.hpp"

/// \namespace Grid_Maker for Grid class which creates all different types of meshes.
namespace Grid_Maker
{
	using namespace dealii;

	/// \brief This object will be used to build meshes over triangulations provided.
	/** This object will be used to build meshes over triangulations provided.  Will
	* 	build meshes for the semiconductor-electrolyte interface problem as well as
	*	meshes for testing.  Can also be used to print meshes to .eps form.
	*/
	template<int dim>	
	class Grid
	{
		public:
			/** Grid object constructor just initializes private data to 
			* 	which portions of the boundary are Dirichlet and Neumann */
			Grid();

			/** Makes a grid which is of the form, \image html semiconductor-grid.png
			*	The top radius is <code> radius one <code/> while the top radius is 
			*	<code> radius two <code/>. It first makes a temporary triangulation
			* refines it, and then prints it out as "temp.msh", then reads it
			* it in again and attaches it to the passed triangulation.  The reason
			* for doing this is so that I can merge this triangulation with another
			* since in order to do that, they must be on the most refined level.
			*/
			void make_semiconductor_grid(Triangulation<dim> 			 & triangulation,
																	ParameterSpace::Parameters & params);	


			/** Makes a grid which is of the form, \image html semiconductor-grid.png
			*	The top radius is <code> radius one <code/> while the top radius is 
			*	<code> radius two <code/>. It first makes a temporary triangulation
			* refines it, and then prints it out as "temp.msh", then reads it
			* it in again and attaches it to the passed triangulation.  The reason
			* for doing this is so that I can merge this triangulation with another
			* since in order to do that, they must be on the most refined level.
			*/
			void make_pn_junction_grid(Triangulation<dim> 			 & triangulation,
																	ParameterSpace::Parameters & params);	
	


			/** Makes a grid which is of the form, \image html electrolyte-grid.png
			*	The top radius is <code> radius one <code/> while the top radius is 
			*	<code> radius two <code/>.  It first makes a temporary triangulation
			* refines it, and then prints it out as "temp.msh", then reads it
			* it in again and attaches it to the passed triangulation.  The reason
			* for doing this is so that I can merge this triangulation with another
			* since in order to do that, they must be on the most refined level.
			*/
			void make_electrolyte_grid(Triangulation<dim> 				 & triangulation,
																	ParameterSpace::Parameters & params);	

			/** Makes a grid which is of the form, \image html Poisson-grid.png 
			*	The top radius is <code> radius one <code/> while the top radius is 
			*	<code> radius two <code/>.   It does so by merging the two provided grids.*/

			/**	@param semiconductor_triang which looks like 
			*	\image html semiconductor-grid.png */ 

			/** @param electrolyte_triang which looks like
 				* \image html electrolyte-grid.png. */	
			void make_merged_grid(const Triangulation<dim>				 & semiconductor_triang,
														const Triangulation<dim>				 & electrolyte_triang,
														Triangulation<dim>							 & merged_triangulation);

			
	 		/** \brief Creates a simple cubic grid with all Dirichlet boundaries. */
			/** Takes <code>triangulation<code/> object and creates a mesh on it.
			* Globally refines the mesh using the refinement level from  
			* <code>params.n_global_refine</code>. 
			*
			* Calculates <code>params.h_min</code>,  <code>params.h_max</code>, and 
			* <code>params.penalty</code> (which is for the LDG method) and stores them in
			* <code>params<code/>.
			*/
			void make_cube(Triangulation<dim> 				& triangulation,
										 const int									& n_global_refine);
			
			/** \brief Subroutines that manually refine the grid. */
			/** Takes <code>triangulation<code/> object and creates a mesh on it.
			* Globally refines the mesh using the refinement level from  
			* <code>params.n_global_refine</code>. 
			* Then it locally refines cells to the level 
			* <code>params.n_local_refine</code>. 
			* The locally refined cells are in specific areas of the mesh which have been 
			* preset in this subroutines source code.  
			*
			* Calculates <code>params.h_min</code>,  <code>params.h_max</code>, and 
			* <code>params.penalty</code> (which is for the LDG method) and stores them in
			* <code>params<code/>.
			*
			*/
			void make_local_refined_cube(Triangulation<dim> 				& triangulation,
										 							 ParameterSpace::Parameters	& params);

			///\brief Subroutine that tags boundaries to be Dirichlet portions.
			/** This subroutine loops over all the cells in <code>triangulation<code/h>
			* and finds which subroutines are on the boundary.  It then
			* flags these faces on the boundary to be <code>Dirichlet<code/>
			* portions of boundary.  
	 		*/
			void make_Dirichlet_boundaries(Triangulation<dim> & triangulation,
																		 ParameterSpace::Parameters	& params);

			/// \brief Subroutine that tags boundaries to be Neumann/Robin portions.
			/** This subroutine loops over all the cells in <code>triangulation<code/>
			* and finds which subroutines are on the boundary.  It then
			* flags these faces to be <code>Neumann<code/> portions of boundary.
			* The choice of which boundary faces are <code>Neumann<code/> is
			* preset in this subroutines source code. 
			*
			* NOTE: BECAREFUL TO MATCH WITH CUBE END POINTS AND DIMENSION.
			* NOTE: <code>Neumann<code/> corresponds to a Neumann bounary condition
			* for Poisson and a Robin boundary condition for the drift-diffusion equation.
			*/
			void make_Neumann_boundaries(Triangulation<dim> & triangulation);

			/// \brief Subroutine that prints the grid into a .msh file
			void output_mesh(Triangulation<dim> & triangulation,
												const std::string & 				grid_name);


			/// \brief Subroutine that prints the grid into a .eps file
			void print_grid(Triangulation<dim> & triangulation,
											const std::string & 				grid_name);

		private:
			enum
			{
				Interface,
				Dirichlet,
				Neumann
			};


			enum
			{
				semiconductor_id,
				electrolyte_id
			};
	};

}

#endif 
