#ifndef _CARRIER_PAIR_H__
#define _CARRIER_PAIR_H__

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>

#include <deal.II/lac/block_sparsity_pattern.h>
#include <deal.II/lac/block_sparse_matrix.h>

#include <string>
#include "Carrier.hpp"

/** \namespace ChargeCarrierSpace Namespace for electron-hole or redox pairs.*/
namespace ChargeCarrierSpace
{
	using namespace dealii;

	/** \brief CarrierPair is the common data between electron-hole or redox pairs.
 	 *
 	 * It contains information like the name of the pair (this is set in the 
 	 * constructor of SOLARCELL::SolarCellProblem) as well as sparsity patterns, 
 	 * information on the mesh, mass matrix for the pair and the electron-hole or
 	 * reductant-oxidant pairs themselves.	*/
	struct CarrierPair
	{
		/** \brief This sets the name of the pair, it is called in constructor of 
 			* SOLARCELL::SolarCellProblem. */
		void set_name(const std::string & str_name)
		{	
			name_of_pair = str_name;
		}

		/** Name of the carrier-pair: electron hole or redox */
		std::string									name_of_pair;

		/** Sparsity pattern for system matrix to be inverted foreach carrier in
 		* 	this pair. */
		SparsityPattern				system_sparsity_pattern;

		/** Sparsity pattern for the mass matrix for each carrier in this pair. */
		SparsityPattern				mass_sparsity_pattern;


		
		/** Constraint matrix for this pair.  This is mostly used to disributed 
 		* 	local dofs to global dofs during constructrion, since for DG methods
 		* 	there is not much to constrain. */
		ConstraintMatrix						constraints;

		/** Mass matrix for the carrier pair.  Note we only need one, this implies
 			*	they use the same time step delta_t. */
		SparseMatrix<double>		mass_matrix;

		/** Carrier 1 should be electron or reductant.*/
		Carrier											carrier_1;
		/** Carrier 2 should be hole or oxidant.*/
		Carrier											carrier_2;

		/** The penalty term for fluxes in LDG.  This is the same for each carrier 
 		*		in a pair since they have the same mesh. */
		double 											penalty;

		/** Time step for each of the carriers.  This is the same.  Any time you
 		*		want to change this you need to update the mass matrix and system matrix.*/
		double											delta_t;

	};

}
#endif
