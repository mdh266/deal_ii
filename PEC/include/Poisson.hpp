#ifndef _Poisson_H__
#define _Poisson_H__

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/block_sparse_matrix.h>
#include <deal.II/lac/block_vector.h>
#include <deal.II/lac/sparse_direct.h>


namespace Poisson
{
	/** \brief Data structures, functions/paramaters, and solver for Poisson
 	*  equation.*/

 /** This object holds the matrix, vectors, constrains and solver for the
 *  mixed finite element approximation to Poisson's equation.  See
 *  MixedPoisson::MixedFEM for more information on the mixed method.
 */
	struct PoissonData
	{

		/// set the direct solver.
		void set_solver()
		{
			solver.initialize(system_matrix);
		}

		/// Solves the linear system and distributes the solution with constraints.
		void solve()
		{
			solver.vmult(solution, system_rhs);
			constraints.distribute(solution);
		}
	
		/// This matrix holds the constraints for hanging nodes and Neumann BC.
		ConstraintMatrix						constraints;
	
		/// Sparsity pattern for the mixed method.
		SparsityPattern				sparsity_pattern;

		/** 	The matrix will be of the form,
			*   \f[ \left[ \begin{matrix}
			*			 A & B \\
			*			 B^{T} & 0 
			*			 \end{matrix} \right]  \f]
			*  See MixedPoisson::MixedFEM for more information on the mixed method.
 			*/
		SparseMatrix<double>		system_matrix;
	
		/** This will be used to store the right hand side of the mixed method,
 			* \f[
 			* = \ 	\frac{1}{\lambda^{2}} \
			* \left(  v,\ \rho_{n}^{e} \ - \ \rho_{p}^{e} \ 
			* -\ \left( \rho_{n} \ - \ \rho_{p} \right) \right) 
			* \f] 
			*  See MixedPoisson::MixedFEM for more information on the mixed method.
 			*/
		Vector<double>					system_rhs;

		/// solution to the linear system.
		Vector<double>					solution;

		/// Direct solver
		SparseDirectUMFPACK					solver;


	};
}

#endif
