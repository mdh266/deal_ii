#ifndef _BOUNDARY_VALUES_H___
#define _BOUNDARY_VALUES_H___

#include <deal.II/base/function.h>
#include <deal.II/lac/vector.h>

using namespace dealii;

///////////////////////////////////////////////////////////////////////////////
// Poisson Boundary Functions
///////////////////////////////////////////////////////////////////////////////

/** \brief The built in bias of the semiconductor \$f\Phi_{\text{bi}}\f$.*/
template <int dim>
class Built_In_Bias : public dealii::Function<dim>
{
	public:
		/** \brief Default constructor.*/
		Built_In_Bias() : dealii::Function<dim>()
		{}
	
		/** \brief Returns value of \$f\Phi_{\text{bi}}\f$ at point p.*/ 
		virtual double value(const dealii::Point<dim> &p, 
								 					const unsigned int component = 0 ) const;
};



/** \brief The applied bias \$f\Phi_{\text{app}}\f$.*/
template <int dim>
class Applied_Bias : public dealii::Function<dim>
{
	public:
		/** \brief Default constructor.*/
		Applied_Bias() : dealii::Function<dim>()
		{}
		
		/** \brief Returns value of \$f\Phi_{\text{app.}}\f$ at point p.*/ 
		virtual double value(const dealii::Point<dim> &p, 
								 					const unsigned int component = 0 ) const;
};



/** \brief The bulk bias of the electrolyte \$f\Phi^{\infty}\f$.*/
template <int dim>
class Bulk_Bias : public dealii::Function<dim>
{
	public:
		/** \brief Default constructor.*/
		Bulk_Bias() : dealii::Function<dim>()
		{}
		
		/** \brief Returns value of \$f\Phi^{\infty}}\f$ at point p.*/ 
		virtual double value(const dealii::Point<dim> &p, 
								 					const unsigned int component = 0 ) const;
};


#endif
