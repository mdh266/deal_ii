///
// Poisson solver using Local Discontinuous Galerkin Method
//
//
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/base/timer.h>
#include <deal.II/base/parameter_handler.h>

#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/block_sparse_matrix.h> // for block structuring
#include <deal.II/lac/block_sparsity_pattern.h>
#include <deal.II/lac/block_vector.h> // for block structuring
#include <deal.II/lac/sparse_direct.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_tools.h>
#include <deal.II/grid/tria_accessor.h>
#include <deal.II/grid/tria_iterator.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_accessor.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_dgq.h> // Lagrange dg fe elements
//#include <deal.II/fe/fe_dgp.h> // Legendre dg fe elements
#include <deal.II/fe/fe_raviart_thomas.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_values.h>

#include <deal.II/numerics/vector_tools.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/data_out.h>

#include <fstream>
#include <iostream>
#include <string>

// multithreading
#include <deal.II/base/work_stream.h>
#include <deal.II/base/multithread_info.h>
#include <deal.II/base/thread_management.h>

#include "Parameters.hpp"
#include "ParameterReader.hpp"
#include "Grid.hpp"
#include "Assembly.hpp"
#include "MixedFEM.hpp"
#include "CarrierPair.hpp"
#include "Poisson.hpp"
#include "InitialConditions.hpp"
#include "Generation.hpp"
#include "BiasValues.hpp"
#include "LDG.hpp"
#include "PostProcessor.hpp"

/** \namespace SOLARCELL This namespace is for everything that has to do particularly 
 *							with the solar cell model is the main namespace for everything that is
 *							constained in the source file for	SOLARCELL:SolarCellProblem. */
namespace SOLARCELL
{
	using namespace dealii;

	/** \brief The recombination model.*/
	/** Schockley-Reed-Hall Recombination functional
 * \f[ \begin{equation}
 *	R(\rho_{n}, \rho_{p})
 * \; = \;
 * \frac{\rho_{i}^{2} \, -\, \rho_{n} \, \rho_{p}}
 * {\tau_{n} \, (\rho_{n} \, + \, \rho_{i}) \, + \, \tau_{p} \, ( \rho_{p} \, + \, \rho_{i})}.
 * \end{equation}
 * \f]
 * 
 * The term \f$\rho_{i}\f$ is the intrinsic electron density and can be spatially varying. 
 * \f$\tau_{n}, \ \tau_{p} \f$ are constants called the electron and hole lifetimes.  
	* @param Holds the intrinsic density and recombination times.
	* @param electron_density \f$\rho_{n}^{k-1}\f$.
	* @param hole_density \f$\rho_{p}^{k-1}\f$.
	*/
	inline	double SRH_Recombination(const double & electron_density,
													 const double & hole_density,
													 const ParameterSpace::Parameters & params)
	{
		return ((params.scaled_intrinsic_density * 
						params.scaled_intrinsic_density - 
						electron_density * hole_density) /
						(params.scaled_electron_recombo_t * (electron_density -
						 params.scaled_intrinsic_density) + 
						(params.scaled_hole_recombo_t * (hole_density -
						 params.scaled_intrinsic_density)) ) );
	}

///////////////////////////////////////////////////////////////////////////////
// Solar Cell Problem Class
///////////////////////////////////////////////////////////////////////////////


	template <int dim>
	class SolarCellProblem
	{
		public:
			SolarCellProblem(const unsigned int degree,
											 ParameterHandler		&param);

			~SolarCellProblem();

			void 
			run();

			/** \brief Solves test Poisson problem and prints convergence rates.*/
			/** Runs mixed method and LDG method sovler for a test Poisson problem
 			*		with Dirichlet boundary conditions.  Computes solutions over a 
 			*		sequence of refined meshes and prints out the convergence rate of the
 			*		methods. */
			void
			test_steady_state(const unsigned int & n_refine,
												ConvergenceTable & Mixed_table,
												ConvergenceTable & LDG_table);

		private:
			// data memembers					
			const unsigned int degree;
			double delta_t;

			enum
			{
				Interface,
				Dirichlet,
				Neumann
			};


			enum
			{
				semiconductor_id,
				electrolyte_id
			};


			ParameterSpace::Parameters					sim_params;
			ParameterSpace::ParameterHandler		&prm;

			/*--------------------------------------------------------------*/
			/* 	Poisson Data Structures																			*/
			/*--------------------------------------------------------------*/
			Triangulation<dim>					Poisson_triangulation;
			DoFHandler<dim>							Poisson_dof_handler;			
			FESystem<dim>								Poisson_fe;
			
			Poisson::PoissonData				Poisson_object;			
			
			/*--------------------------------------------------------------*/
			/* 	Carrier Data Structures																			*/
			/*--------------------------------------------------------------*/
			Triangulation<dim>								semiconductor_triangulation;
			DoFHandler<dim>										semiconductor_dof_handler;
			ChargeCarrierSpace::CarrierPair 	electron_hole_pair;

			Triangulation<dim>								electrolyte_triangulation;
			DoFHandler<dim>										electrolyte_dof_handler;
			ChargeCarrierSpace::CarrierPair 	redox_pair;

			FESystem<dim>											carrier_fe;

			MixedPoisson::MixedFEM<dim>				Mixed_Assembler;
			LDG_System::LDG<dim>							LDG_Assembler;	

			/*-------------------------------------------------------------*/
			/*	Initial Conditions																				 */
 			/*-------------------------------------------------------------*/
			const	Electrons_Equilibrium<dim>	electrons_e;
			const Holes_Equilibrium<dim>			holes_e;
			const Reductants_Equilibrium<dim>	reductants_e;
			const Oxidants_Equilibrium<dim>		oxidants_e;


			/*-------------------------------------------------------------*/
			/* The potential functions																		 */
			/*-------------------------------------------------------------*/
			const Built_In_Bias<dim>					built_in_bias;
			const Applied_Bias<dim>						applied_bias;
			const Bulk_Bias<dim>							bulk_bias;


			/*-------------------------------------------------------------*/
			/* Generation function																				 */
 			/*-------------------------------------------------------------*/
			Generation<dim>						 generation;

			// member functions
	
			/** \brief Distribute the degrees of freedom for each 
 			*		differential equation*/
			void 
			distribute_dofs();

			/** \brief Allocate the memeory for the matrices and 
			 * 	vectors for this problem.*/
			void 
			allocate_memory();	
	
			/** \brief Allocate the memeory for the block matrices and 
 			* 	vectors for this problem.*/
			void 
			allocate_block__memory();	
	

			/** Assembles the matrix corresponding to the mixed method for Poisson eq.
			* See MixedPoisson::MixedFEM for more information.*/
			void 
			assemble_Poisson_matrix();

			/** Copys the local matrices assembled my and MixedFEM object into
			* the global matrix which is in Poisson_object.  
			* Used for assembly in parallel by workstream.
			*/
			void 
			copy_local_to_global_Poisson_matrix(
											const Assembly::Poisson::CopyData<dim> 	& data);

			/** Assemble the global right hand side for Poisson's test case.*/		
			void 
			assemble_test_Poisson_rhs();

			/** Copys the local right hand side vectors assembled my and this
			*  object into the global right hand side vector in Poisson_object. 
			*  Used for assembly in parallel by workstream.
			*/
			void 
			copy_local_to_global_Poisson_rhs(
				const Assembly::Poisson::CopyData<dim> 								& data);

			/** \brief Assembles the global poisson rhs for the coupled problem.*/
 			/** It first uses the semiconductor_dof_handler to run over the 
 			* cells in the semiconductor triangulation and calls 
 			* SOLARCELL::SolarCellProblem::assemble_local_Poisson_rhs_for_semiconductor 
 			* using workstream. It then uses the electrolyte_dof_handler to run over the cells
 			* in the electrolyte triangulation and calls 
 			* SOLARCELL::SolarCellProblem::assemble_local_Poisson_rhs_for_electrolyte
 			* using workstream.
 			*
 			* The general problem for the Poisson equation is, 
			* \f[ \begin{align}
			*	\epsilon_{r}^{-1} \ \textbf{D} \ + \ \nabla \Phi \ &= \ 0  && \text{in} \; \Omega \\
			*	\ \nabla \ \cdot \ \textbf{D} \  &= \frac{1}{\lambda^{2}}
			*	 \ f(\textbf{x})					&& \text{in} \; \Omega \\
			*		\textbf{D} \ \cdot \ \boldsymbol \eta \ &= \ 0 
			*						&& \text{on} \; \partial \Omega_{N} \\
			*		\Phi \ &=  \ \Phi_{\text{app}} \ + \ \Phi_{\text{bi}}
			*							 && \text{on} \;  \Gamma_{S} \\
			*		\Phi \ &=  \ \Phi^{\infty}
			*							 && \text{on} \;  \Gamma_{E} 
			*   \end{align} \f]
			*
			* For \f$\lambda^{2} \ = \ \frac{\Phi^{*} \epsilon }{L^{2} q C^{*}}\f$. \f].  
			* \f$f(\textbf{x})\f$ is defined as
			* \f[ 
			* f(\textbf{x}) \ = \ 
			* \left\{ \begin{array}{cc}
			* \left[ \ N_{D} \ - \ N_{A} \ -\  \left( \rho_{n} \ - \ \rho_{p} \right) \ \right]   
			* & \text{in} \; \Omega_{S} \\
			*  -\  \left( \rho_{r} \ - \ \rho_{o} \right)   
			* & \text{in} \; \Omega_{E}
			* \end{array} \right.
			* \f] 
			* 
			* See MixedPoisson::MixedFEM on how to build the corresponding left hand side of the
			* weak formulation.  This function essentially constructs:
			*
			* \f[ = \ -\langle \textbf{p} \ , \ \Phi_{\text{app}} + \Phi_{\text{bi}} 
			* \rangle_{\Gamma_{S}} - \langle \textbf{p} \ , \Phi^{\infty} \ \rangle_{\Gamma_{E}}   
			*	- \frac{1}{\lambda^{2}} \
			* \left(  v,\ f(\textbf{x}) \right)_{\Omega} \f]
			* 
			*
			*  For all \f$( v \  , \ \textbf{p}  ) \, \in \, W \, \times\, \textbf{V}^{d}\f$. 
			*/
			void
			assemble_Poisson_rhs();

			/**\brief Assembles the local poisson rhs for the coupled problem
 			*		in the semiconductor triangulation. */
			/**  This function loops through all the cells in the 
 			*	semiconductor triangulation and performs the following calculation, 
 			*	\f[ \ = \ -\langle \textbf{p} \ , \ \Phi_{\text{app}} + \Phi_{\text{bi}} 
			* \rangle_{\partial \Omega_{e} \cap \Gamma_{S}} 
			*	- \frac{1}{\lambda^{2}} \
			*	\left( v, 
			* \left[ \ N_{D} \ - \ N_{A} \ -\  \left( \rho_{n} \ - \ \rho_{p} \right) \ \right]   
			* \ \right)_{\Omega_{e}} \f]
			* 
			*
			* For all \f$( v \  , \ \textbf{p}  ) \, \in \, W \, \times\, \textbf{V}^{d}\f$ and
			* all \f$\Omega_{e} \in \Omega_{S}\f$. It stores the data in 
			*	Assembly::Poisson::CopyData.
			*/
			void
			assemble_local_Poisson_rhs_for_semiconductor(
				const typename DoFHandler<dim>::active_cell_iterator 	& cell,
				Assembly::AssemblyScratch<dim>											  & scratch,
				Assembly::Poisson::CopyData<dim>											& data);
	
			/** \brief Assembles the local poisson rhs for the coupled problem
 			*		in the electrolyte triangulation. */
			/**  This function loops through all the cells in the 
 			*	electrolyte triangulation and performs the following calculation, 
 			*	\f[ \ = \ -\langle \textbf{p} \ , \ \Phi^{\infty} 
			* \rangle_{\partial \Omega_{e} \cap \Gamma_{E}} 
			*	- \frac{1}{\lambda^{2}} \
			*	\left( v, 
			* \left[  -\  \left( \rho_{r} \ - \ \rho_{o} \right) \ \right]   
			* \ \right)_{\Omega_{e}} \f]
			* 
			*
			*  For all \f$( v \  , \ \textbf{p}  ) \, \in \, W \, \times\, \textbf{V}^{d}\f$ and
			*  all \f$\Omega_{e} \in \Omega_{E}\f$. It stores the data in 
			*  Assembly::Poisson::CopyData. */
			void 
			assemble_local_Poisson_rhs_for_electrolyte(
				const typename DoFHandler<dim>::active_cell_iterator 	& cell,
				Assembly::AssemblyScratch<dim>											  & scratch,
				Assembly::Poisson::CopyData<dim>											& data);
	

			/** Assembles the mass matrices for electron-hole and redox pairs*/
			void
			assemble_mass_matrix();

			
			/** Copies the local calculations into the global mass matrix for the
 			*		electron hole pair. */
			void
			copy_local_to_global_semiconductor_mass_matrix(						
						const Assembly::DriftDiffusion::CopyData<dim>			& data);

			/** Copies the local calculations into the global mass matrix for the
 			*		redox pair. */
			void
			copy_local_to_global_electrolyte_mass_matrix(						
						const Assembly::DriftDiffusion::CopyData<dim>			& data);


			/** Assembles the matrices corresponding to solid integrals as well 
 			*		as the matrices which correspond to fluxes on the boundary.  This
 			*		assembles everything in parallel, and does so for both electron-hole
 			*		pairs and redox pairs.*/
			void
			assemble_LDG_cell_and_bc_terms(const double & transient_or_steady);
	
			/** Copies the local calculations into the global system matrix for the
 			*		electron hole pair. */
			void
			copy_local_to_global_semiconductor_system_matrix(						
						const Assembly::DriftDiffusion::CopyData<dim>			& data);

			/** Copies the local calculations into the global system matrix for the
 			*		redox pair. */
			void
			copy_local_to_global_electrolyte_system_matrix(						
						const Assembly::DriftDiffusion::CopyData<dim>			& data);

			/** Copies the local calculations into the global right hand side for the
 			*		electron hole pair. */
			void
			copy_local_to_global_semiconductor_system_rhs(						
						const Assembly::DriftDiffusion::CopyData<dim>			& data);

			/** Copies the local calculations into the global right hand side for the
 			*		redox pair. */
			void
			copy_local_to_global_electrolyte_system_rhs(						
						const Assembly::DriftDiffusion::CopyData<dim>			& data);

			/** \brief Assembles the RHS for Poissons equation using the LDG method.*/
			void
			assemble_test_LDG_rhs();	

			/** Builds the RHSes for the electron and hole equations.*/
			/**	This function loops through all the cells in the semiconductor
 			*	triangulation. It does so by using semiconductor_dof_handler and 
 			*	calling SOLARCELL::SolarCellProblem::assemble_local_semiconductor_rhs 
 			*	uising workstream. For more information on the LDG method used in this 
 			*	caee see LDG_System::LDG.  */
			void
			assemble_semiconductor_rhs();
		
			/** Builds the local RHSes for the electron and hole equations.*/
			/** It stores the calculated data in Assembly::DriftDiffusion::CopyData
 			*	the  rhs at time \f$k\f$ is,
 			*	\f[ = 
			*	\left( v ,  R(u^{k-1})  + G(\textbf{x}) \right)_{\Omega_{e}} - 
			*	\langle   v, K( u^{k-1})    \rangle_{\partial \Omega_{e} \cap \Sigma}  
			*	 +
			*	\left( s \textbf{P}  \cdot \boldsymbol \nabla \Phi , u^{k-1} 
			*	\right)_{\Omega_{e}}
			*	- 
			*	\langle  \textbf{p} ,  u_{D} \rangle_{ \partial \Omega_{e} \cap \Gamma_{S} }
			*	\f]
			*
			*  For all \f$(v,\textbf{p}) \in W \times \textbf{W}^{d})\f$ and all 
			*  \f$\Omega_{e} \in \Omega_{S} \f$.
 			*/
			void
			assemble_local_semiconductor_rhs(
								const typename DoFHandler<dim>::active_cell_iterator & cell,
								Assembly::AssemblyScratch<dim>											 & scratch,
								Assembly::DriftDiffusion::CopyData<dim>							 & data);

			/** Builds the RHSes for the reductant and oxidant equations.*/
			/**	This function loops through all the cells in the electrolyte
 			*	triangulation. It does so by using electrolyte_dof_handler and 
 			*	calling SOLARCELL::SolarCellProblem::assemble_local_electrolyte_rhs 
 			*	uising workstream.*/
			void
			assemble_electrolyte_rhs();
		
			/** Builds the local RHSes for the electron and hole equations.*/
			/** It stores the calculated data in Assembly::DriftDiffusion::CopyData
 			*	the  rhs at time \f$k\f$ is,
 			*	\f[ = 		
			*	- \langle   v, K( u^{k-1})    \rangle_{\partial \Omega_{e} \cap \Sigma}  
			*	 +
			*	\left( s \textbf{P}  \cdot \boldsymbol \nabla \Phi , u^{k-1} 
			*	\right)_{\Omega_{e}}
			*	- 
			*	\langle  \textbf{p} ,  u_{D} \rangle_{ \partial \Omega_{e} \cap \Gamma_{E} }
			*	\f]
			*
			*  For all \f$(v,\textbf{p}) \in W \times \textbf{W}^{d})\f$ and all 
			*  \f$\Omega_{e} \in \Omega_{E} \f$.
 			*/
			void
			assemble_local_electrolyte_rhs(
								const typename DoFHandler<dim>::active_cell_iterator & cell,
								Assembly::AssemblyScratch<dim>											 & scratch,
								Assembly::DriftDiffusion::CopyData<dim>							 & data);


			/** Factorizes all the matrices (Poisson and carriers). */	
			void
			set_solvers();

			/** Solves Poisson equation.*/
			void 
			solve_Poisson();

			/** Solve the linear systems of electron and holes.*/
			void
			solve_semiconductor_system();

			/** Solve the linear systems of reductants and oxidants.*/
			void
			solve_electrolyte_system();

			/** Prints the output in scaled form. */
			void
			output_results();
	};

}
